﻿using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using DistributionNetwork.dto;
using DistributionNetwork.gdi.primitives;
using DistributionNetwork.styles;
using PhotonGraphics.elements.baseElements;
using PhotonGraphics.elements.shapes;

namespace TestGraphicsApp {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
            canvas1.RenderElement = createTestGdiObject();
            canvas1.fitSize();
        }

        private GdiElement createTestGdiObject() {
            var group = new GdiGroup();
            var staticRectList = new List<GdiRectangle>();
            var ellipsesList = new List<GdiEllipse>();
            for (int i = 0; i < 4; i++) {
                var rect = new GdiRectangle
                {
                    Size = new SizeF(120, 70),
                    DrawBorder = true,
                    BorderPen = new Pen(Color.Black, 5),
                    DrawFill = true,
                    ShapeBrush = new SolidBrush(Color.Aqua),
                    Text = string.Format("static rect {0}", i + 1),
                    HasConstantSize = true,
                    HasConstantAngle = true
                };
                rect.MouseHover += onRectMouseHover;
                rect.MouseLeave += onRectMouseLeave;
                rect.MouseUp += onRectMouseUp;
                rect.MouseDown += onRectMouseDown;
                staticRectList.Add(rect);

                ellipsesList.Add(new GdiEllipse
                {
                    Size = new SizeF(20, 20),
                    DrawBorder = false,
                    DrawFill = true,
                    ShapeBrush = new SolidBrush(Color.Red),
                    HasConstantSize = false
                });
            }

            staticRectList[1].Location = new PointF(500, 0);
            staticRectList[2].Location = new PointF(0, 300);
            staticRectList[3].Location = new PointF(500, 300);

            staticRectList[0].AnchorPoint = ellipsesList[0].AnchorPoint = ellipsesList[0].Center = new PointF(120, 70);
            staticRectList[1].AnchorPoint = ellipsesList[1].AnchorPoint = ellipsesList[1].Center = new PointF(500, 70);
            staticRectList[2].AnchorPoint = ellipsesList[2].AnchorPoint = ellipsesList[2].Center = new PointF(120, 300);
            staticRectList[3].AnchorPoint = ellipsesList[3].AnchorPoint = ellipsesList[3].Center = new PointF(500, 300);

            staticRectList[0].rotate(30);
            staticRectList[1].rotate(40);
            staticRectList[2].rotate(50);
            staticRectList[3].rotate(60);
            staticRectList[3].scale(2,2);

            var blob =
              new ParamsBlob(
                  new InfoDto() {
                      Title = "Test",
                      State = ObjectState.osEnabled,
                      Name = "TEst",
                      Params = new List<InfoParamDto>() { new InfoParamDto() { Name = "Абрикосцы", Units = "шт", Value = 2 } , new InfoParamDto() { Name = "Гаитяни", Units = "шт", Value = 2 } }
                  }, StyleCollection.groupBlobParamsStyle);
            blob.Direct = ParamsBoubleDirect.RightDown;
            blob.Location = new PointF(493, 302);
            blob.rotate(60);
            blob.HasConstantAngle = true;
            blob.ZOrder = 1;

           var blob2 =
           new ParamsBlob(
               new InfoDto() {
                   Title = "Test",
                   State = ObjectState.osEnabled,
                   Name = "TEst",
                   Params = new List<InfoParamDto>() { new InfoParamDto() { Name = "Абрикосцы", Units = "шт", Value = 2 }, new InfoParamDto() { Name = "Гаитяни", Units = "шт", Value = 2 } }
               }, StyleCollection.groupBlobParamsStyle);
            blob2.Direct = ParamsBoubleDirect.RightUp;
            blob2.Location = new PointF(500, 65);
            blob2.rotate(40);
            blob2.HasConstantAngle = true;
            blob2.IsConstantElement = true;
            blob2.ZOrder = 1;

            var blob3 =
            new ParamsBlob(
                new InfoDto() {
                    Title = "Test",
                    State = ObjectState.osEnabled,
                    Name = "TEst",
                    Params = new List<InfoParamDto>() { new InfoParamDto() { Name = "Абрикосцы", Units = "шт", Value = 2 }, new InfoParamDto() { Name = "Гаитяни", Units = "шт", Value = 2 } }
                }, StyleCollection.groupBlobParamsStyle);
            blob3.Direct = ParamsBoubleDirect.LeftUp;
            blob3.Location = new PointF(120, 70);
            blob3.HasConstantAngle = true;
            blob3.rotate(30);
            blob3.ZOrder = 1;

            var blob4 =
           new ParamsBlob(
               new InfoDto() {
                   Title = "Test",
                   State = ObjectState.osEnabled,
                   Name = "TEst",
                   Params = new List<InfoParamDto>() { new InfoParamDto() { Name = "Абрикосцы", Units = "шт", Value = 2 }, new InfoParamDto() { Name = "Гаитяни", Units = "шт", Value = 2 } }
               }, StyleCollection.groupBlobParamsStyle);
            blob4.Direct = ParamsBoubleDirect.LeftDown;
            blob4.Location = new PointF(118, 304);
            blob4.HasConstantAngle = true;
            blob4.rotate(40);
            blob4.HasConstantSize = true;
            blob4.ZOrder = 1;

            // blob.rotate(30);
            // blob2.rotate(30);
            // blob3.rotate(30);
            // blob4.rotate(30);

            group.childs.Add(blob);
            group.childs.Add(blob2);
            group.childs.Add(blob3);
            group.childs.Add(blob4);

            group.childs.Add(new GdiImage() { Location = new PointF(0,0), Size = new SizeF(40,80)});

            group.childs.AddRange(ellipsesList);
           
            group.childs.Add(new GdiRoundedRectangle
            {
                Size = new SizeF(380, 230),
                Location = new PointF(120, 70),
                Radius = 30,
                DrawBorder = true,
                BorderPen = new Pen(Color.Black, 10),
                DrawFill = true,
                ShapeBrush = new SolidBrush(Color.Chocolate),
                Text = "zoomable rect",
                Font = new Font("Segoe UI", 30, FontStyle.Bold),
                Enabled = true,
                HasConstantLineThickness = true,
                ZOrder = 60
//                AutoSize = true,
//                AnchorPoint = new PointF(310, 185)
            });
            
            group.childs.AddRange(staticRectList);
            
            group.TransformationMatrix.Rotate(-10);

            var constantShape = new GdiRectangle
            {
                Size = new SizeF(200, 100),
                DrawBorder = true,
                BorderPen = new Pen(Color.Black, 5),
                DrawFill = true,
                ShapeBrush = new SolidBrush(Color.Chartreuse),
                Text = "constant figure",
            };
            constantShape.MouseHover += (_, __) => constantShape.ShapeBrush = new SolidBrush(Color.CadetBlue);
            constantShape.MouseLeave += (_, __) => constantShape.ShapeBrush = new SolidBrush(Color.Chartreuse);
            constantShape.MouseDown += (_, __) => constantShape.ShapeBrush = new SolidBrush(Color.Aqua);
            constantShape.MouseUp += (_, __) => constantShape.ShapeBrush = new SolidBrush(Color.CadetBlue);
            constantShape.IsConstantElement = true;
            constantShape.rotate(10);
            group.childs.Add(constantShape);

            var line = new GdiPolyline
            {
                Enabled = true,
                ActiveDistanceFactor = 2f,
                HasConstantLineThickness = true,
                Pen = new Pen(Color.Blue, 7),
                Points = new List<PointF>
                {
                    new PointF(100, 100),
                    new PointF(400, 200),
                    new PointF(100, 300),
                    new PointF(400, 400),
                    new PointF(100, 500),
                    new PointF(400, 600)
                },
//                ZOrder = -1
            };
            line.MouseHover += (_, __) => line.Pen = new Pen(Color.Red, 7);
            line.MouseLeave += (_, __) => line.Pen = new Pen(Color.Blue, 7);
            group.childs.Add(line);

            return group;
        }

        private void onRectMouseDown(object sender, MouseEventArgs e) {
            var rect = sender as GdiRectangle;
            if (rect != null) {
                rect.ShapeBrush = new SolidBrush(Color.Maroon);
            }
        }

        private void onRectMouseUp(object sender, MouseEventArgs e) {
            var rect = sender as GdiRectangle;
            if (rect != null) {
                rect.ShapeBrush = new SolidBrush(Color.Red);
            }
        }

        private void onRectMouseHover(object sender, MouseEventArgs e) {
            var rect = sender as GdiRectangle;
            if (rect != null) {
                rect.ShapeBrush = new SolidBrush(Color.Red);
            }
        }

        private void onRectMouseLeave(object sender, MouseEventArgs e) {
            var rect = sender as GdiRectangle;
            if (rect != null) {
                rect.ShapeBrush = new SolidBrush(Color.Aqua);
            }
        }

        private void buttonFitSizeClick(object sender, System.EventArgs e) {
            canvas1.fitSize();
        }
    }
}