﻿namespace TestGraphicsApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.canvas1 = new PhotonGraphics.Canvas();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonFitSize = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // canvas1
            // 
            this.canvas1.BackColor = System.Drawing.Color.Gold;
            this.canvas1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.canvas1.Location = new System.Drawing.Point(0, 31);
            this.canvas1.Name = "canvas1";
            this.canvas1.RenderElement = null;
            this.canvas1.Size = new System.Drawing.Size(844, 476);
            this.canvas1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonFitSize);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(844, 31);
            this.panel1.TabIndex = 1;
            // 
            // buttonFitSize
            // 
            this.buttonFitSize.Location = new System.Drawing.Point(3, 5);
            this.buttonFitSize.Name = "buttonFitSize";
            this.buttonFitSize.Size = new System.Drawing.Size(49, 23);
            this.buttonFitSize.TabIndex = 0;
            this.buttonFitSize.Text = "fit size";
            this.buttonFitSize.UseVisualStyleBackColor = true;
            this.buttonFitSize.Click += new System.EventHandler(this.buttonFitSizeClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 507);
            this.Controls.Add(this.canvas1);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Test";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private PhotonGraphics.Canvas canvas1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonFitSize;
    }
}

