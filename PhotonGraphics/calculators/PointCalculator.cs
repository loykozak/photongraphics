﻿using System;
using System.Drawing;

namespace PhotonGraphics.calculators
{
    public static class PointCalculator
    {
        public static Point toPoint(this PointF pt) {
            return new Point((int) pt.X, (int) pt.Y);
        }

        public static PointF toPointF(this Point pt) {
            return new PointF(pt.X, pt.Y);
        }

        public static PointF multiply(this PointF pt, float value) {
            return new PointF(pt.X * value, pt.Y * value);
        }

        public static PointF multiply(this PointF pt, double value) {
            return pt.multiply((float) value);
        }

        public static PointF add(this PointF pt, PointF pt2) {
            return pt.add(pt2.X, pt2.Y);
        }

        public static PointF add(this PointF pt, float x, float y) {
            return new PointF(pt.X + x, pt.Y + y);
        }

        public static PointF add(this PointF pt, double x, double y) {
            return new PointF(pt.X + (float) x, pt.Y + (float) y);
        }

        public static PointF substract(this PointF pt, PointF pt2) {
            return pt.add(-pt2.X, -pt2.Y);
        }

        public static float length(this PointF pt) {
            return (float) Math.Sqrt(pt.X * pt.X + pt.Y * pt.Y);
        }

        private static PointF getSplinePoint(PointF pt1, PointF pt2, PointF pt1R, PointF pt2R, float t) {
            var t2 = t * t;
            var t3 = t2 * t;
            return pt1
                .multiply(2 * t3 - 3 * t2 + 1)
                .add(pt1R.multiply(t3 - 2 * t2 + t))
                .add(pt2.multiply(-2 * t3 + 3 * t2))
                .add(pt2R.multiply(t3 - t2));
        }

        public static PointF[] getSplinePoints(PointF pt1, PointF pt2, PointF pt1R, PointF pt2R, int pointsCount) {
            if (pointsCount <= 1) {
                return null;
            }
            var arr = new PointF[pointsCount];
            var delta = 1f / (pointsCount - 1);
            var t = 0f;
            for (int i = 0; i < pointsCount; i++) {
                arr[i] = getSplinePoint(pt1, pt2, pt1R, pt2R, t);
                t += delta;
            }
            return arr;
        }
    }
}