﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace PhotonGraphics.calculators {
    /// <summary>
    /// Расчетчик для графики
    /// </summary>
    public static class GdiCalculator {
        /// <summary>
        /// Расчет толщины линии для объектов с постоянной толщиной линий
        /// </summary>
        /// <param name="lineThickness">Толщина линии из конфигурации объекта</param>
        /// <param name="zoomFactor">коэффициент приближения камеры</param>
        /// <returns></returns>
        public static float calcConstantLineThickness(float lineThickness, float zoomFactor) {
            return lineThickness/zoomFactor;
        }

        /// <summary>
        /// Перенос объекта
        /// </summary>
        /// <param name="matrix">матрица преобразования</param>
        /// <param name="dx">перенос по X</param>
        /// <param name="dy">перенос по Y</param>
        public static void translate(Matrix matrix, float dx, float dy) {
            if (dx.Equals(0) &&
                dy.Equals(0)) {
                return;
            }
            matrix.Translate(dx, dy);
        }

        /// <summary>
        /// Изменение размера объекта
        /// </summary>
        /// <param name="matrix">матрица преобразования</param>
        /// <param name="sx">изменение размера по X</param>
        /// <param name="sy">изменение размера по Y</param>
        /// <param name="anchorPoint">точка якоря</param>
        public static void scale(Matrix matrix, float sx, float sy, PointF anchorPoint) {
            if (sx.Equals(1) &&
                sy.Equals(1)) {
                return;
            }
            translate(matrix, anchorPoint.X, anchorPoint.Y);
            matrix.Scale(sx, sy);
            translate(matrix, -anchorPoint.X, -anchorPoint.Y);
        }

        /// <summary>
        /// Поворот объекта
        /// </summary>
        /// <param name="matrix">матрица преобразования</param>
        /// <param name="angle">угол поворота в градусах</param>
        /// <param name="anchorPoint">точка якоря</param>
        public static void rotate(Matrix matrix, float angle, PointF anchorPoint) {
            if (angle.Equals(0)) {
                return;
            }
            translate(matrix, anchorPoint.X, anchorPoint.Y);
            matrix.Rotate(angle);
            translate(matrix, -anchorPoint.X, -anchorPoint.Y);
        }

        /// <summary>
        /// Вычисление рассояния от точки до отрезка
        /// </summary>
        /// <param name="point"></param>
        /// <param name="segmentBeginPoint"></param>
        /// <param name="segmentEndPoint"></param>
        /// <returns></returns>
        public static float calcDistancePointToSegment(PointF point, PointF segmentBeginPoint, PointF segmentEndPoint) {
            Func<PointF, PointF, float> dot = (pt1, pt2) => pt1.X*pt2.X + pt1.Y*pt2.Y;
            Func<PointF, float> norm = pt => (float) Math.Sqrt(dot(pt, pt));
            Func<PointF, PointF, PointF> vect = (pt1, pt2) => new PointF(pt2.X - pt1.X, pt2.Y - pt1.Y);
            Func<PointF, PointF, float> dist = (pt1, pt2) => norm(vect(pt1, pt2));

            var v = vect(segmentBeginPoint, segmentEndPoint);
            var w = vect(segmentBeginPoint, point);

            var c1 = dot(w, v);
            if (c1 <= 0) {
                return dist(point, segmentBeginPoint);
            }

            var c2 = dot(v, v);
            if (c2 <= c1) {
                return dist(point, segmentEndPoint);
            }

            var b = c1/c2;
            var pointB = new PointF(segmentBeginPoint.X + v.X*b, segmentBeginPoint.Y + v.Y*b);
            return dist(point, pointB);
        }
    }
}