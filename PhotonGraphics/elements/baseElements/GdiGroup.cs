﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace PhotonGraphics.elements.baseElements {
    /// <summary>
    /// Группа графических элементов
    /// </summary>
    public class GdiGroup : GdiElement {
        #region Данные

        public readonly List<GdiElement> childs;

        #endregion

        #region Свойства

        /*
        public override PointF Location
        {
            get
            {
                if (childs.Count(i => i.Visible) == 0) {
                    return base.Location;
                }
                var childsLocation = ChildsLocation;
                return new PointF(base.Location.X + childsLocation.X, base.Location.Y + childsLocation.Y);
            }
            set
            {
                var oldPos = Location;
                float dx = value.X - oldPos.X;
                float dy = value.Y - oldPos.Y;
                base.Location = new PointF(base.Location.X + dx, base.Location.Y + dy);
            }
        }
        */

        public PointF ChildsLocation
        {
            get
            {
                var realBounds = childs.Where(i => i.Visible).Select(i => i.RealBound).ToList();
                return new PointF(realBounds.Min(i => i.Left), realBounds.Min(i => i.Top));
            }
        }

        public override SizeF Size
        {
            get
            {
                var realBounds = childs.Where(c => !c.IsConstantElement && c.Visible).Select(i => i.RealBound).ToList();
                if (realBounds.Count == 0) {
                    return new SizeF();
                }
                var childsLeftTopPoint = new PointF(realBounds.Min(i => i.Left),
                    realBounds.Min(i => i.Top));
                var childsRightBottomPoint = new PointF(realBounds.Max(i => i.Right),
                    realBounds.Max(i => i.Bottom));
                return new SizeF(childsRightBottomPoint.X - childsLeftTopPoint.X,
                    childsRightBottomPoint.Y - childsLeftTopPoint.Y);
            }
            set { }
        }

        #endregion

        public GdiGroup() {
            childs = new List<GdiElement>();
        }

        public override void renderPrepare(LibGraphicsObject go, int parentZOrder) {
            renderPushMatrix(go);
            var z = parentZOrder + ZOrder;
            foreach (var element in childs) {
                element.renderPrepare(go, z);
            }
            renderPopMatrix(go);
        }
    }
}