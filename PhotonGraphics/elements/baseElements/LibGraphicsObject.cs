﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;

namespace PhotonGraphics.elements.baseElements
{
    /// <summary>
    /// Объект графики с преобразованиями
    /// </summary>
    public class LibGraphicsObject
    {
        private readonly List<Matrix> _transformationStack;

        private List<RenderElementItem> _renderItems;

        #region Свойства

        /// <summary>
        /// Графика GDI
        /// </summary>
        public Graphics Graphics { get; set; }

        /// <summary>
        /// Коэффициент приближения камеры
        /// </summary>
        public float ZoomFactor { get; set; }

        #endregion


        #region Конструктор

        public LibGraphicsObject(Graphics graphics, float zoomFactor) {
            Graphics = graphics;
            ZoomFactor = zoomFactor;
            _transformationStack = new List<Matrix> { Graphics.Transform };
            _renderItems = new List<RenderElementItem>();
        }

//        public LibGraphicsObject()
//        {
//            _transformationStack = new List<Matrix>();
//            _renderItems = new List<RenderElementItem>();
//        }

        #endregion


        #region Методы

        public void pushMatrix(Matrix matrix) {
            var m = getFinalMatrix().Clone();
            m.Multiply(matrix, MatrixOrder.Prepend);
            _transformationStack.Add(m);
        }

        public void popMatrix() {
            if (_transformationStack.Count > 1) {
                _transformationStack.RemoveAt(_transformationStack.Count - 1);
            }
        }

        public Matrix getFinalMatrix() {
            if (_transformationStack.Count > 0) {
                return _transformationStack[_transformationStack.Count - 1];
            }
            return new Matrix();
        }

        public void addElement(GdiElement element, int zOrder) {
            _renderItems.Add(new RenderElementItem
            {
                ZOrder = zOrder,
                Element = element,
            });
        }

        public void setRenderObject(GdiElement renderObject) {
            _renderItems.Clear();
            if (renderObject == null) {
                return;
            }
            renderObject.renderPrepare(this, 0);
            _renderItems = _renderItems.OrderBy(i => i.ZOrder).ToList();
        }

        public void render() {
            foreach (var renderElementItem in _renderItems) {
                if (renderElementItem.Element.Visible) {
                    Graphics.Transform = renderElementItem.Element.FinalMatrix;
                    renderElementItem.Element.render(this);
                }
            }
        }

        #endregion

    }

    public class RenderElementItem {
        public int ZOrder { get; set; }

        public GdiElement Element { get; set; }
    }
}
