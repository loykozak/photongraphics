﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PhotonGraphics.elements.baseElements
{
    /// <summary>
    /// Активный графический элемент с событиями
    /// </summary>
    public abstract class GdiActiveElement : GdiElement
    {
        private bool _isHovered;
        private bool _enabled = true;

        public event EventHandler<MouseEventArgs> MouseClick;
        public event EventHandler<MouseEventArgs> MouseDoubleClick;
        public event EventHandler<MouseEventArgs> MouseDown;
        public event EventHandler<MouseEventArgs> MouseUp;
        public event EventHandler<MouseEventArgs> MouseHover;
        public event EventHandler<MouseEventArgs> MouseLeave;

        public ActiveElementState MouseState { get; set; }

        /// <summary>
        /// Активность элемента
        /// </summary>
        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        /// <summary>
        /// Порядок отрисовки
        /// </summary>
        public int GroupZOrder { get; set; }

        /// <summary>
        /// Проверка нахождения точки с экранными координатами внутри области отрисовки элемента
        /// </summary>
        /// <param name="screenX"></param>
        /// <param name="screenY"></param>
        /// <returns></returns>
        public bool isScreenCoordinatesOnElement(int screenX, int screenY) {
            if (FinalMatrix == null) {
                return false;
            }
            var matr = FinalMatrix.Clone();
            matr.Invert();
            var mousePoint = new PointF(screenX, screenY);
            var pointsArr = new[] {mousePoint};
            matr.TransformPoints(pointsArr);
            return isPointOnElement(pointsArr[0]);
        }

        /// <summary>
        /// Проверка нахождения точки с локальными координатами внутри области отрисовки элемента
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        protected virtual bool isPointOnElement(PointF point) {
            return RenderingRect.Contains(point);
        }

        public bool processMouseHovering(MouseEventArgs eventArgs, bool hovered /*, ref bool firstHovered*/) {
//            if (Enabled &&
//                hovered &&
//                _isHovered &&
//                !firstHovered) {
//                firstHovered = true;
//            }
            if (_isHovered == hovered ||
                !Enabled) {
                return false;
            }
            _isHovered = hovered;
            if (MouseState != ActiveElementState.aesActive) {
                MouseState = _isHovered ? ActiveElementState.aesHovered : ActiveElementState.aesInactive;
            }
            if (hovered) {
//                if (!firstHovered) {
//                    firstHovered = true;
                return raiseMouseHover(eventArgs);
//                }
//                return false;
            }
            return raiseMouseLeave(eventArgs);
        }

        public bool raiseMouseClick(MouseEventArgs eventArgs) {
            return raiseMouseEvent(MouseClick, eventArgs);
        }

        public bool raiseMouseDoubleClick(MouseEventArgs eventArgs) {
            return raiseMouseEvent(MouseDoubleClick, eventArgs);
        }

        public bool raiseMouseDown(MouseEventArgs eventArgs) {
            MouseState = ActiveElementState.aesActive;
            return raiseMouseEvent(MouseDown, eventArgs);
        }

        public bool raiseMouseUp(MouseEventArgs eventArgs) {
            MouseState = _isHovered ? ActiveElementState.aesHovered : ActiveElementState.aesInactive;
            return raiseMouseEvent(MouseUp, eventArgs);
        }

        private bool raiseMouseHover(MouseEventArgs eventArgs) {
            return raiseMouseEvent(MouseHover, eventArgs);
        }

        private bool raiseMouseLeave(MouseEventArgs eventArgs) {
            return raiseMouseEvent(MouseLeave, eventArgs);
        }

        private bool raiseMouseEvent(EventHandler<MouseEventArgs> eventHandler, MouseEventArgs eventArgs) {
            if (!Enabled) {
                return false;
            }
            var h = eventHandler;
            if (h != null) {
                h(this, eventArgs);
                return true;
            }
            return false;
        }
    }
}