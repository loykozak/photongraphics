namespace PhotonGraphics.elements.baseElements
{
    public enum ActiveElementState
    {
        aesInactive,
        aesActive,
        aesHovered
    }
}
