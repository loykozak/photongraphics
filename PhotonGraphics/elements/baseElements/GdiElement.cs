﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using PhotonGraphics.calculators;

namespace PhotonGraphics.elements.baseElements {
    /// <summary>
    /// Базовый графический элемент
    /// </summary>
    public class GdiElement {
        #region Поля

        private bool _visible = true;
        private Matrix _transformationMatrix = new Matrix();

        #endregion

        #region Свойства

        /// <summary>
        /// Матрица преобразований
        /// </summary>
        public Matrix TransformationMatrix
        {
            get { return _transformationMatrix; }
            set
            {
                if (value == null) {
                    return;
                }
                _transformationMatrix = value;
            }
        }

        /// <summary>
        /// Окончательная матрица преобразования как произведение всех матриц
        /// </summary>
        public Matrix FinalMatrix { get; set; }

        /// <summary>
        /// Положение элемента (верхний левый угол)
        /// </summary>
        public virtual PointF Location
        {
            get
            {
                return TransformationMatrix != null
                    ? new PointF(TransformationMatrix.OffsetX, TransformationMatrix.OffsetY) : new PointF();
            }
            set
            {
                var dx = value.X - TransformationMatrix.OffsetX;
                var dy = value.Y - TransformationMatrix.OffsetY;
                GdiCalculator.translate(TransformationMatrix, dx, dy);
            }
        }

        /// <summary>
        /// Центральная точка элемента
        /// </summary>
        public virtual PointF Center
        {
            get { return new PointF(Location.X + Size.Width/2, Location.Y + Size.Height/2); }
            set { Location = new PointF(value.X - Size.Width/2, value.Y - Size.Height/2); }
        }

        /// <summary>
        /// Размер элемента
        /// </summary>
        public virtual SizeF Size { get; set; }

        /// <summary>
        /// Прямоугольная область для отрисовки
        /// </summary>
        protected RectangleF RenderingRect
        {
            get { return new RectangleF(new PointF(), Size); }
        }

        /// <summary>
        /// Прямоугольная область с учетом трансформации
        /// </summary>
        public virtual RectangleF RealBound
        {
            get
            {
                if ( /*CorrectionMatrix == null && */isMatrixWithTranslationsOnly(TransformationMatrix)) {
                    return new RectangleF(Location, Size);
                }
                var pointsArr = new[]
                {
                    new PointF(0, 0),
                    new PointF(Size.Width, 0),
                    new PointF(0, Size.Height),
                    new PointF(Size.Width, Size.Height)
                };
//                if (CorrectionMatrix != null) {
//                    var m = CorrectionMatrix.Clone();
//                    m.Multiply(TransformationMatrix, MatrixOrder.Prepend);
//                    m.TransformPoints(pointsArr);
//                } else {
                TransformationMatrix.TransformPoints(pointsArr);
//                }
                var minX = pointsArr.Min(i => i.X);
                var minY = pointsArr.Min(i => i.Y);
                var maxX = pointsArr.Max(i => i.X);
                var maxY = pointsArr.Max(i => i.Y);
                return new RectangleF(minX, minY, maxX - minX, maxY - minY);
            }
        }

        /// <summary>
        /// Имеет ли элемент постоянную позицию, размер и угол поворота, не зависящие от преобразований вне элемента
        /// </summary>
        public bool IsConstantElement { get; set; }

        /// <summary>
        /// Имеет ли элемент постоянный размер, не зависящий от преобразований вне элемента
        /// </summary>
        public bool HasConstantSize { get; set; }

        /// <summary>
        /// Имеет ли элемент постоянный угол поворота, не зависящий от преобразований вне элемента
        /// </summary>
        public bool HasConstantAngle { get; set; }

        /// <summary>
        /// Имеет ли элемент постоянную толщину линий, не зависящую от зума
        /// </summary>
        public bool HasConstantLineThickness { get; set; }

        /// <summary>
        /// Точка привязки элемента к окружению (для режимов HasConstantSize и/или HasConstantAngle)
        /// </summary>
        public PointF AnchorPoint { get; set; }

        /// <summary>
        /// Порядок отрисовки
        /// </summary>
        public int ZOrder { get; set; }

        /// <summary>
        /// Коэффициент для вычисления ширины линии в зависимости от зума
        /// </summary>
        protected float BorderPenWidthFactor { get; private set; }
        

        /// <summary>
        /// Видимость элемента
        /// </summary>
        public bool Visible
        {
            get { return _visible; }
            set { _visible = value; }
        }

        /// <summary>
        /// Тэг
        /// </summary>
        public object Tag { get; set; }

        #endregion

        #region Конструктор

        protected GdiElement() {
            TransformationMatrix = new Matrix();
        }

        #endregion

        /// <summary>
        /// Перенос объекта
        /// </summary>
        /// <param name="dx">перенос по X</param>
        /// <param name="dy">перенос по Y</param>
        public void translate(float dx, float dy) {
            GdiCalculator.translate(TransformationMatrix, dx, dy);
        }

        /// <summary>
        /// Изменение размера объекта
        /// </summary>
        /// <param name="sx">изменение размера по X</param>
        /// <param name="sy">изменение размера по Y</param>
        public void scale(float sx, float sy) {
            var location = Location;
            GdiCalculator.scale(TransformationMatrix, sx, sy,
                new PointF(AnchorPoint.X - location.X, AnchorPoint.Y - location.Y));
        }

        /// <summary>
        /// Поворот объекта
        /// </summary>
        /// <param name="angle">угол поворота в градусах</param>
        public void rotate(float angle) {
            var location = Location;
            GdiCalculator.rotate(TransformationMatrix, angle,
                new PointF(AnchorPoint.X - location.X, AnchorPoint.Y - location.Y));
        }

        /// <summary>
        /// Убрать все трансформирующие изменения элемента
        /// </summary>
        public void clearTransformation() {
            TransformationMatrix = new Matrix();
        }

        public virtual void render(LibGraphicsObject go) {
        }

        public virtual void renderPrepare(LibGraphicsObject go, int parentZOrder) {
            renderPushMatrix(go);
            go.addElement(this, parentZOrder + ZOrder);
            if (HasConstantLineThickness) {
                BorderPenWidthFactor = GdiCalculator.calcConstantLineThickness(1f, go.ZoomFactor);
            } else {
                BorderPenWidthFactor = 1f;
            }
            renderPopMatrix(go);
        }

        protected void renderPushMatrix(LibGraphicsObject go) {
            if (IsConstantElement ||
                HasConstantSize ||
                HasConstantAngle) {
                var correctionMatrix = new Matrix();
                if (IsConstantElement) {
                    correctionMatrix = go.getFinalMatrix().Clone();
                    correctionMatrix.Invert();
                } else {
                    var parentMatrixElements = go.getFinalMatrix().Elements;
                    if (HasConstantSize) {
                        GdiCalculator.scale(correctionMatrix, 1/parentMatrixElements[0], 1/parentMatrixElements[3],
                            AnchorPoint);
                    }
                    if (HasConstantAngle) {
                        var parentAngle =
                            (float) (Math.Atan2(parentMatrixElements[2], parentMatrixElements[0])*180/Math.PI);
                        GdiCalculator.rotate(correctionMatrix, parentAngle, AnchorPoint);
                    }
                }
                go.pushMatrix(correctionMatrix);
            } 
            if (!isMatrixIdentity(TransformationMatrix)) {
                go.pushMatrix(TransformationMatrix);
            }
            FinalMatrix = go.getFinalMatrix().Clone();
        }

        protected void renderPopMatrix(LibGraphicsObject go) {
            if (IsConstantElement ||
                HasConstantSize ||
                HasConstantAngle) {
                go.popMatrix();
            }
            if (!isMatrixIdentity(TransformationMatrix)) {
                go.popMatrix();
            }
        }

        /// <summary>
        /// Проверяет является ли матрица единичной.
        /// Стандартный метод Matrix.IsIdentity почему-то не работает
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        protected static bool isMatrixIdentity(Matrix matrix) {
            return isMatrixWithTranslationsOnly(matrix) &&
                   matrix.Elements[4].Equals(0f) &&
                   matrix.Elements[5].Equals(0f);
        }

        /// <summary>
        /// Содержит ли матрица только преобразования трансляции 
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        protected static bool isMatrixWithTranslationsOnly(Matrix matrix) {
            return matrix.Elements[0].Equals(1f) &&
                   matrix.Elements[1].Equals(0f) &&
                   matrix.Elements[2].Equals(0f) &&
                   matrix.Elements[3].Equals(1f);
        }

        /// <summary>
        /// Возвращает перо для отображения линий
        /// </summary>
        /// <param name="borderPen">оригинальное перо объекта</param>
        /// <param name="go">инфо о графике</param>
        /// <returns></returns>
        protected Pen getBorderPen(Pen borderPen, LibGraphicsObject go) {
            if (!HasConstantLineThickness) {
                return borderPen;
            }
            var pen = (Pen) borderPen.Clone();
            pen.Width = pen.Width * BorderPenWidthFactor;
            return pen;
        }
    }
}