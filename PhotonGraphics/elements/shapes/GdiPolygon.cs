﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using PhotonGraphics.elements.baseElements;

namespace PhotonGraphics.elements.shapes {
    public class GdiPolygon : GdiShape {
        public List<PointF> Points { get; set; }

        public override RectangleF RealBound
        {
            get
            {
                if (Points == null ||
                    Points.Count == 0) {
                    return new RectangleF();
                }
                var pointsArr = Points.ToArray();
                if (!isMatrixIdentity(TransformationMatrix)) {
                    TransformationMatrix.TransformPoints(pointsArr);
                }
                var minX = pointsArr.Min(i => i.X);
                var minY = pointsArr.Min(i => i.Y);
                var maxX = pointsArr.Max(i => i.X);
                var maxY = pointsArr.Max(i => i.Y);
                return new RectangleF(minX, minY, maxX - minX, maxY - minY);
            }
        }

        public GdiPolygon() {
            Points = new List<PointF>();
        }

        public override void render(LibGraphicsObject go) {
            if (Points != null &&
                Points.Count > 0) {
                var parr = Points.ToArray();
                if (DrawFill && ShapeBrush != null) {
                    go.Graphics.FillPolygon(ShapeBrush, parr);
                }
                if (DrawBorder && BorderPen != null) {
                    go.Graphics.DrawPolygon(getBorderPen(BorderPen, go), parr);
                }
            }
            base.render(go);
        }
    }
}