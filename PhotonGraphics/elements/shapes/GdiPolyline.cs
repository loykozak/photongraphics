﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using PhotonGraphics.calculators;
using PhotonGraphics.elements.baseElements;

namespace PhotonGraphics.elements.shapes {
    public class GdiPolyline : GdiActiveElement {
        private GdiArrow _beginArrow;
        private GdiArrow _endArrow;
        private Pen _pen;

        public List<PointF> Points { get; set; }

        public override RectangleF RealBound
        {
            get
            {
                if (Points == null ||
                    Points.Count == 0) {
                    return new RectangleF();
                }
                var pointsArr = Points.ToArray();
                if (!isMatrixIdentity(TransformationMatrix)) {
                    TransformationMatrix.TransformPoints(pointsArr);
                }
                var minX = pointsArr.Min(i => i.X);
                var minY = pointsArr.Min(i => i.Y);
                var maxX = pointsArr.Max(i => i.X);
                var maxY = pointsArr.Max(i => i.Y);
                return new RectangleF(minX, minY, maxX - minX, maxY - minY);
            }
        }

        public Pen Pen
        {
            get { return _pen; }
            set
            {
                _pen = value;
                if (_beginArrow != null) {
                    _beginArrow.Color = _pen.Color;
                }
                if (_endArrow != null) {
                    _endArrow.Color = _pen.Color;
                }
            }
        }

        /// <summary>
        /// Коэффициент для дистанции активности элемента
        /// </summary>
        public float ActiveDistanceFactor { get; set; }

        public GdiArrow BeginArrow
        {
            get { return _beginArrow; }
            set
            {
                _beginArrow = value;
                updateArrowOnBegin();
            }
        }

        public GdiArrow EndArrow
        {
            get { return _endArrow; }
            set
            {
                _endArrow = value;
                updateArrowOnEnd();
            }
        }

        public GdiPolyline() {
            Points = new List<PointF>();
            Pen = new Pen(Color.Black);
            Enabled = false;
            ActiveDistanceFactor = 1f;
        }

        public override void render(LibGraphicsObject go) {
            if (Pen != null &&
                Points != null &&
                Points.Count > 0) {
                go.Graphics.DrawPath(getBorderPen(Pen, go),
                    new GraphicsPath(Points.ToArray(), Points.Select(p => (byte) PathPointType.Line).ToArray()));
                if (Points.Count > 1) {
                    _beginArrow?.render(go);
                    _endArrow?.render(go);
                }
            }
        }

        protected override bool isPointOnElement(PointF point) {
            if (Points.Count <= 1) {
                return false;
            }
            var maxDistance = Pen.Width/2*BorderPenWidthFactor*ActiveDistanceFactor;
            for (int i = 1; i < Points.Count; i++) {
                if (GdiCalculator.calcDistancePointToSegment(point, Points[i - 1], Points[i]) <= maxDistance) {
                    return true;
                }
            }
            return false;
        }

        public void update() {
            updateArrowOnBegin();
            updateArrowOnEnd();
        }

        private void updateArrowOnBegin() {
            if (Points == null ||
                Points.Count < 2 ||
                _beginArrow == null) {
                return;
            }
            _beginArrow.Location = Points[0];
            _beginArrow.Angle = getArrowAngle(Points[0], Points[1]);
        }

        private void updateArrowOnEnd() {
            if (Points == null ||
                Points.Count < 2 ||
                _endArrow == null) {
                return;
            }
            _endArrow.Location = Points[Points.Count - 1];
            _endArrow.Angle = getArrowAngle(Points[Points.Count - 1], Points[Points.Count - 2]);
        }

        private float getArrowAngle(PointF arrowPoint, PointF nextPoint) {
            float dx = arrowPoint.X - nextPoint.X;
            float dy = arrowPoint.Y - nextPoint.Y;
            return (float) Math.Atan2(dy, dx);
        }
    }
}