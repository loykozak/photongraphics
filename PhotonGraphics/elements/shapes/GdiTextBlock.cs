﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using PhotonGraphics.elements.baseElements;

namespace PhotonGraphics.elements.shapes {
    public class GdiTextBlock : GdiActiveElement {
        #region Свойства

        /// <summary>
        /// Размер текстового блока после автоподстройки
        /// </summary>
        public SizeF TextAutoSize
        {
            get
            {
                var textSize = calcTextSize(Canvas.measurementGraphics);
                textSize.Width += TextPadding.Left + TextPadding.Right;
                textSize.Height += TextPadding.Top + TextPadding.Bottom;
                return textSize;
            }
        }

        /// <summary>
        /// Шрифт
        /// </summary>
        public Font Font
        {
            get
            {
                if (TextBlocks == null ||
                    TextBlocks.Count == 0) {
                    return null;
                }
                return TextBlocks[0].Font;
            }
            set
            {
                initTextBlocks();
                TextBlocks[0].Font = value;
            }
        }

        /// <summary>
        /// Кисть
        /// </summary>
        public Brush TextBrush
        {
            get
            {
                if (TextBlocks == null ||
                    TextBlocks.Count == 0) {
                    return null;
                }
                return TextBlocks[0].Brush;
            }
            set
            {
                initTextBlocks();
                TextBlocks[0].Brush = value;
            }
        }

        /// <summary>
        /// Текст
        /// </summary>
        public string Text
        {
            get
            {
                if (TextBlocks == null ||
                    TextBlocks.Count == 0) {
                    return null;
                }
                return TextBlocks[0].Text;
            }
            set
            {
                initTextBlocks();
                TextBlocks[0].Text = value;
            }
        }

        /// <summary>
        /// Отступ текста от границ элемента
        /// </summary>
        public Padding TextPadding { get; set; }

        /// <summary>
        /// Вертикальное выравнивание
        /// </summary>
        public VerticalAlignment VerticalAlignment { get; set; }

        /// <summary>
        /// Горизонтальное выравнивание
        /// </summary>
        public HorizontalAlignment HorizontalAlignment { get; set; }

        /// <summary>
        /// Куски текстового блока
        /// </summary>
        public List<TextBlockInfo> TextBlocks { get; set; }

        #endregion

        #region Конструктор

        public GdiTextBlock() {
            TextPadding = Padding.Empty;
            VerticalAlignment = VerticalAlignment.Center;
            HorizontalAlignment = HorizontalAlignment.Center;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Подстройка размеров текстового блока под содержимое
        /// </summary>
        public void adjustSize() {
            if (TextBlocks != null)
            {
                var textSize = TextAutoSize;
                if (Size.Width.Equals(0) ||
                    Size.Height.Equals(0))
                {
                    Size = new SizeF(1, 1);
                }
                var deltaWidth = Size.Width - textSize.Width;
                var deltaHeight = Size.Height - textSize.Height;
                var location = Location;
                var relatedAnchorPoit = new PointF(AnchorPoint.X - location.X, AnchorPoint.Y - location.Y);
                var deltaX = deltaWidth*relatedAnchorPoit.X/Size.Width;
                var deltaY = deltaHeight*relatedAnchorPoit.Y/Size.Height;
                location.X += deltaX;
                location.Y += deltaY;
                Location = location;
                Size = textSize;
            }
        }

        private void initTextBlocks() {
            if (TextBlocks == null) {
                TextBlocks = new List<TextBlockInfo>();
            }
            if (TextBlocks.Count == 0) {
                TextBlocks.Add(new TextBlockInfo
                {
                    Font = new Font(FontFamily.GenericSansSerif, 12),
                    Brush = Brushes.Black
                });
            }
        }

        public override void render(LibGraphicsObject go) {
            renderText(go);
        }

        protected void renderText(LibGraphicsObject go) {
            if (TextBlocks == null)
            {
                return;
            }
            var renderedBlocks = getRenderedBlocks();
            if (renderedBlocks.Count == 0)
            {
                return;
            }

            var rect = RenderingRect;
            var textBoundRect = new RectangleF(rect.Left + TextPadding.Left,
                rect.Top + TextPadding.Top,
                rect.Width - TextPadding.Left - TextPadding.Right,
                rect.Height - TextPadding.Top - TextPadding.Bottom);
            if (renderedBlocks.Count == 1)
            {
                renderSingleBlock(go.Graphics, textBoundRect, renderedBlocks[0]);
                return;
            }
            renderedBlocks = splitRenderedBlocks(renderedBlocks);
            renderManyBlocks(go.Graphics, textBoundRect, renderedBlocks);
        }

        private void renderSingleBlock(Graphics g, RectangleF boundRect, TextBlockInfo textBlock) {
            var stringFormat = new StringFormat
            {
                Alignment = HorizontalAlignment.getStringAlignment(),
                LineAlignment = VerticalAlignment.getStringAlignment()
            };
            g.DrawString(textBlock.Text, textBlock.Font, textBlock.Brush, boundRect, stringFormat);
        }

        private void renderManyBlocks(Graphics g, RectangleF boundRect, List<TextBlockInfo> textBlocks) {

            var blockStrings = getTextBlockStringList(textBlocks, g);

            var blockSize = new SizeF(blockStrings.Max(i => i.Width),
                blockStrings.Sum(i => i.Height));

            var startPoint = new PointF();
            switch (HorizontalAlignment) {
                case HorizontalAlignment.Left:
                    startPoint.X = boundRect.X;
                    break;
                case HorizontalAlignment.Right:
                    startPoint.X = boundRect.X + boundRect.Width - blockSize.Width;
                    break;
                default:
                    startPoint.X = boundRect.X + boundRect.Width/2 - blockSize.Width/2;
                    break;
            }
            switch (VerticalAlignment) {
                case VerticalAlignment.Top:
                    startPoint.Y = boundRect.Y;
                    break;
                case VerticalAlignment.Bottom:
                    startPoint.Y = boundRect.Y + boundRect.Height - blockSize.Height;
                    break;
                default:
                    startPoint.Y = boundRect.Y + boundRect.Height/2 - blockSize.Height/2;
                    break;
            }

            foreach (var textBlockString in blockStrings) {
                var lineOffset = startPoint.X;
                switch (HorizontalAlignment) {
                    case HorizontalAlignment.Right:
                        lineOffset = startPoint.X + blockSize.Width - textBlockString.Width;
                        break;
                    case HorizontalAlignment.Center:
                        lineOffset = startPoint.X + blockSize.Width/2 - textBlockString.Width/2;
                        break;
                }
                foreach (var textBlockWithSize in textBlockString.blocks) {
                    var block = textBlockWithSize.Block;
                    var rect = new RectangleF(new PointF(lineOffset, startPoint.Y), textBlockWithSize.Size);
                    g.DrawString(block.Text, block.Font, block.Brush, rect);
                    lineOffset += textBlockWithSize.Size.Width;
                }
                startPoint.Y += textBlockString.Height;
            }
        }

        public SizeF calcTextSize(SizeF layoutArea = default(SizeF)) {
            return calcTextSize(Canvas.measurementGraphics, layoutArea);
        }

        private SizeF calcTextSize(Graphics graphics, SizeF layoutArea = default(SizeF)) {
            var renderedBlocks = getRenderedBlocks();
            if (renderedBlocks.Count == 0) {
                return new SizeF();
            }
            if (renderedBlocks.Count == 1) {
                var textBlock = renderedBlocks[0];
                if (layoutArea.Equals(default(SizeF))) {
                    return graphics.MeasureString(textBlock.Text, textBlock.Font);
                }
                return graphics.MeasureString(textBlock.Text, textBlock.Font, layoutArea);
            }
            renderedBlocks = splitRenderedBlocks(renderedBlocks);
            var blockStrings = getTextBlockStringList(renderedBlocks, graphics);
            return new SizeF(blockStrings.Max(i => i.Width),
                blockStrings.Sum(i => i.Height));
        }

        private List<TextBlockInfo> getRenderedBlocks() {
            return TextBlocks.Where(i => !string.IsNullOrEmpty(i.Text) && i.Font != null && i.Brush != null).ToList();
        }

        private static List<TextBlockInfo> splitRenderedBlocks(List<TextBlockInfo> blocks) {
            return blocks
                .SelectMany(i => {
                    var strings = i.Text.Split('\n');
                    var result = strings
                        .Select(s => new TextBlockInfo
                        {
                            Text = s,
                            Font = i.Font,
                            Brush = i.Brush,
                            EndLine = true
                        })
                        .ToList();
                    result.Last().EndLine = false;
                    return result;
                })
                .ToList();
        }

        private static List<TextBlockString> getTextBlockStringList(List<TextBlockInfo> blocks, Graphics g) {
            var sizesAggregate = blocks
                .Select(i => new TextBlockWithSize
                {
                    Block = i,
                    Size = g.MeasureString(i.Text, i.Font)
                })
                .ToList();

            var blockStrings = new List<TextBlockString> { new TextBlockString() };
            while (sizesAggregate.Count > 0)
            {
                var block = sizesAggregate[0];
                sizesAggregate.RemoveAt(0);
                blockStrings.Last().blocks.Add(block);
                if (block.Block.EndLine &&
                    sizesAggregate.Count > 0)
                {
                    blockStrings.Add(new TextBlockString());
                }
            }
            return blockStrings;
        }

        #endregion
    }

    internal class TextBlockWithSize {
        public TextBlockInfo Block { get; set; }
        public SizeF Size { get; set; }
    }

    internal class TextBlockString {
        public readonly List<TextBlockWithSize> blocks = new List<TextBlockWithSize>();

        public float Width
        {
            get { return blocks.Sum(i => i.Size.Width); }
        }

        public float Height
        {
            get { return blocks.Max(i => i.Size.Height); }
        }
    }

    internal static class GdiTextBlockExtension {
        public static StringAlignment getStringAlignment(this VerticalAlignment alignment) {
            switch (alignment) {
                case VerticalAlignment.Top:
                    return StringAlignment.Far;
                case VerticalAlignment.Bottom:
                    return StringAlignment.Near;
                default:
                    return StringAlignment.Center;
            }
        }

        public static StringAlignment getStringAlignment(this HorizontalAlignment alignment) {
            switch (alignment) {
                case HorizontalAlignment.Left:
                    return StringAlignment.Near;
                case HorizontalAlignment.Right:
                    return StringAlignment.Far;
                default:
                    return StringAlignment.Center;
            }
        }
    }

    /// <summary>
    /// Информация о текстовом блоке
    /// </summary>
    public class TextBlockInfo {
        /// <summary>
        /// Шрифт текстового блока
        /// </summary>
        public Font Font { get; set; }

        /// <summary>
        /// Кисть текстового блока
        /// </summary>
        public Brush Brush { get; set; }

        /// <summary>
        /// Строка текстового блока
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Завершать строку с переводом на новую
        /// </summary>
        public bool EndLine { get; set; }
    }
}