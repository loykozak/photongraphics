﻿using PhotonGraphics.elements.baseElements;

namespace PhotonGraphics.elements.shapes {
    public class GdiRectangle : GdiShape {
        public override void render(LibGraphicsObject go) {
            var rect = RenderingRect;
            if (DrawFill && ShapeBrush != null) {
                go.Graphics.FillRectangle(ShapeBrush, rect);
            }
            if (DrawBorder && BorderPen != null) {
                go.Graphics.DrawRectangle(getBorderPen(BorderPen, go), rect.X, rect.Y, rect.Width, rect.Height);
            }
            base.render(go);
        }
    }
}