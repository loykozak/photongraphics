﻿using System;
using System.Drawing;
using System.Windows.Forms;
using PhotonGraphics.elements.baseElements;

namespace PhotonGraphics.elements.shapes {
    public class GdiImage : GdiActiveElement {

        public Image Image { get; set; }

        public PictureBoxSizeMode SizeMode { get; set; }

        public GdiImage() {
            SizeMode = PictureBoxSizeMode.AutoSize;
        }

        public override void render(LibGraphicsObject go) {
            if (Image == null) return;
            var g = go.Graphics;
            var location = new PointF();
            switch (SizeMode) {
                case PictureBoxSizeMode.AutoSize:
                    Size = Image.Size;
                    g.DrawImage(Image, location);
                    break;
                case PictureBoxSizeMode.CenterImage:
                    var ci = new PointF(Image.Width/2f, Image.Height/2f);
                    var cb = new PointF(Size.Width/2f, Size.Height/2f);
                    var offset = new PointF(ci.X - cb.X, ci.Y - cb.Y);
                    var irect = new RectangleF(offset, Size);
                    g.DrawImage(Image, RenderingRect, irect, GraphicsUnit.Pixel);
                    break;
                case PictureBoxSizeMode.Normal:
                    g.DrawImage(Image, RenderingRect, RenderingRect, GraphicsUnit.Pixel);
                    break;
                case PictureBoxSizeMode.StretchImage:
                    g.DrawImage(Image, location.X, location.Y, Size.Width, Size.Height);
                    break;
                case PictureBoxSizeMode.Zoom:
                    var ratio = Math.Min(Size.Width/Image.Width, Size.Height/Image.Height);
                    var width = Image.Width*ratio;
                    var heigt = Image.Height*ratio;
                    g.DrawImage(Image, location.X + (Size.Width - width)/2, location.Y + (Size.Height - heigt)/2, width,
                        heigt);
                    break;

            }
        }
    }
}
