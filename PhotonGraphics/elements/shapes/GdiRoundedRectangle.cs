﻿using System.Drawing;
using System.Drawing.Drawing2D;
using PhotonGraphics.elements.baseElements;

namespace PhotonGraphics.elements.shapes {
    public class GdiRoundedRectangle : GdiShape {
        public float Radius { get; set; }

        public override void render(LibGraphicsObject go) {
            var figure = makeRoundedRect(RenderingRect, Radius);
            if (DrawFill && ShapeBrush != null) {
                go.Graphics.FillPath(ShapeBrush, figure);
            }
            if (DrawBorder && BorderPen != null) {
                go.Graphics.DrawPath(getBorderPen(BorderPen, go), figure);
            }
            base.render(go);
        }

        private static GraphicsPath makeRoundedRect(RectangleF bounds, float radius) {
            var diameter = radius*2;
            var size = new SizeF(diameter, diameter);
            var arc = new RectangleF(bounds.Location, size);
            var path = new GraphicsPath();

            if (radius <= 0) {
                path.AddRectangle(bounds);
                return path;
            }

            // top left arc  
            path.AddArc(arc, 180, 90);

            // top right arc  
            arc.X = bounds.Right - diameter;
            path.AddArc(arc, 270, 90);

            // bottom right arc  
            arc.Y = bounds.Bottom - diameter;
            path.AddArc(arc, 0, 90);

            // bottom left arc 
            arc.X = bounds.Left;
            path.AddArc(arc, 90, 90);

            path.CloseFigure();
            return path;
        }
    }
}