﻿using System.Drawing;
using PhotonGraphics.elements.baseElements;

namespace PhotonGraphics.elements.shapes {
    /// <summary>
    /// Стрелка линии
    /// </summary>
    public class GdiArrow : GdiElement {
        #region Константы

        private const float DEFAULT_HEIGHT = 5f;
        private const float DEFAULT_WIDTH = 3f;
        private const float DEFAULT_MIDDLE_INSET = 0.5f;
        private static readonly Color DefaultColor = Color.Black;

        #endregion

        #region Данные

        private Color _color;
        private Brush _brush;
        private PointF[] _points = new PointF[4];
        private float _height;
        private float _width;
        private float _middleInset;
        private float _angle;

        #endregion

        #region Свойства

   
        /// <summary>
        /// Цвет стрелки
        /// </summary>
        public Color Color {
            get { return _color; }
            set {
                _color = value;
                _brush = new SolidBrush(_color);
            }
        }

        /// <summary>
        /// Высота стрелки
        /// </summary>
        public float Height {
            get { return _height; }
            set {
                _height = value;
                update();
            }
        }

        /// <summary>
        /// Ширина стрелки
        /// </summary>
        public float Width {
            get { return _width; }
            set {
                _width = value;
                update();
            }
        }

        /// <summary>
        /// Серединное искревление стрелки
        /// </summary>
        public float MiddleInset {
            get { return _middleInset; }
            set {
                _middleInset = value;
                update();
            }
        }

        /// <summary>
        /// Угол поворота стрелки в радианах
        /// </summary>
        public float Angle {
            get { return _angle; }
            set
            {
                TransformationMatrix.Rotate(-_angle);
                _angle = value;
                TransformationMatrix.Rotate(_angle);
            }
        }

        public override PointF Center
        {
            get { return Location; }
            set { Location = value; }
        }

        #endregion

        public GdiArrow() {
            Color = DefaultColor;
            _height = DEFAULT_HEIGHT;
            _width = DEFAULT_WIDTH;
            _middleInset = DEFAULT_MIDDLE_INSET;
            _angle = 0;
             update();
        }

        public override void render(LibGraphicsObject go) {
            go.Graphics.FillPolygon(_brush, _points);
        }

        private void update() {
            _points[0] = new PointF(0, 0);
            _points[1] = new PointF(-Height, -Width/2);
            _points[2] = new PointF(-Height + MiddleInset, 0);
            _points[3] = new PointF(-Height, Width/2);
        }
    }
}