﻿using PhotonGraphics.elements.baseElements;

namespace PhotonGraphics.elements.shapes {
    public class GdiEllipse : GdiShape {
        public override void render(LibGraphicsObject go) {
            var rect = RenderingRect;
            if (DrawFill && ShapeBrush != null) {
                go.Graphics.FillEllipse(ShapeBrush, rect);
            }
            if (DrawBorder && BorderPen != null) {
                go.Graphics.DrawEllipse(getBorderPen(BorderPen, go), rect);
            }
            base.render(go);
        }
    }
}