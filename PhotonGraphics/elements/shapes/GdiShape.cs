﻿using System.Drawing;

namespace PhotonGraphics.elements.shapes
{
    public class GdiShape : GdiTextBlock
    {
        public bool DrawBorder { get; set; }

        public bool DrawFill { get; set; }

        public Pen BorderPen { get; set; }

        public Brush ShapeBrush { get; set; }

        public GdiShape() {
            DrawBorder = true;
            DrawFill = true;
            BorderPen = new Pen(Color.Black);
            ShapeBrush = Brushes.BurlyWood;
        }
    }
}
