﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;
using PhotonGraphics.elements.baseElements;

namespace PhotonGraphics
{
    [ToolboxItem(true)]
    public partial class Canvas : UserControl
    {
        #region Данные

        private static readonly Image MeasurementGraphicsContext = new Bitmap(1, 1);
        public static readonly Graphics measurementGraphics;

        private readonly GdiGroup _canvasGroup = new GdiGroup();

        private GdiElement _renderElement;

        private const float ZOOM_FACTOR_KOEFF = 1.2f;
        private const float ZOOM_FACTOR_MAX = 100f;
        private const float ZOOM_FACTOR_MIN = 0.1f;

        private bool _allowMouseZoom = true;
        private bool _allowMouseTranslate = true;

        private float _zoomFactor = 1f;
        private PointF _translation = new PointF(0, 0);
        private PointF _startTranslation;
        private Point _startTranslationPoint;
        private bool _translateProcess;

        private List<GdiActiveElement> _activeElements;
        private bool _activeHovered;

        #endregion

        #region Свойства

        [Browsable(true)]
        [DefaultValue(true)]
        public bool AllowMouseZoom
        {
            get { return _allowMouseZoom; }
            set {
                if (_allowMouseZoom == value) {
                    return;
                }
                _allowMouseZoom = value;
                if (value) {
                    MouseWheel += gdiRenderCtrlMouseWheel;
                }
                else {
                    MouseWheel -= gdiRenderCtrlMouseWheel;
                }
            }
        }

        [Browsable(true)]
        [DefaultValue(true)]
        public bool AllowMouseTranslate
        {
            get { return _allowMouseTranslate; }
            set { _allowMouseTranslate = value; }
        }

        /// <summary>
        /// Отступ по оси X при автомасштабе
        /// </summary>
        [Browsable(true)]
        [DefaultValue(10)]
        public int OffsetX { get; set; } = 10;

        /// <summary>
        /// Отступ по оси Y при автомасштабе
        /// </summary>
        [Browsable(true)]
        [DefaultValue(10)]
        public int OffsetY { get; set; } = 10;

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public GdiElement RenderElement
        {
            get { return _renderElement; }
            set {
                _renderElement = value;
                _canvasGroup.childs.Clear();
                _canvasGroup.childs.Add(_renderElement);
                updateAvtiveElements();
                Refresh();
            }
        }

        [Browsable(false)]
        public RectangleF ViewPort
        {
            get {
                var leftTopPoint = getWorldPoint(0, 0);
                var rightBottomPoint = getWorldPoint(Width, Height);
                return new RectangleF(leftTopPoint,
                    new SizeF(rightBottomPoint.X - leftTopPoint.X, rightBottomPoint.Y - leftTopPoint.Y));
            }
        }

        #endregion

        #region События

        /// <summary>
        /// Событие изменения размера либо местоположения видимой области
        /// </summary>
        public event EventHandler ViewPortChanged;

        #endregion

        #region Конструкторы

        static Canvas() {
            measurementGraphics = Graphics.FromImage(MeasurementGraphicsContext);
        }

        public Canvas() {
            initControl();
            InitializeComponent();
        }

        #endregion

        #region Методы

        public void updateGraphis() {
            RenderElement = RenderElement;
        }

        private void initControl() {
            DoubleBuffered = true;
            MouseWheel += gdiRenderCtrlMouseWheel;
            MouseDown += gdiRenderCtrlMouseDown;
            MouseUp += gdiRenderCtrlMouseUp;
            MouseMove += gdiRenderCtrlMouseMove;
            MouseClick += gdiRenderCtrlMouseClick;
            MouseDoubleClick += gdiRenderCtrlMouseDoubleClick;
            Resize += gdiRenderCtrlResize;

            _activeElements = new List<GdiActiveElement>();
        }

        public PointF getWorldPoint(int mouseX, int mouseY) {
            var worldPoint = new PointF(mouseX / _zoomFactor, mouseY / _zoomFactor);
            worldPoint.X -= _translation.X;
            worldPoint.Y -= _translation.Y;
            return worldPoint;
        }

        public PointF getWorldPoint(Point mousePoint) {
            return getWorldPoint(mousePoint.X, mousePoint.Y);
        }

        private void updateAvtiveElements() {
            _activeElements.Clear();
            addActiveElements(_renderElement, 0);
            _activeElements = _activeElements.OrderByDescending(i => i.GroupZOrder).ToList();
        }

        private void addActiveElements(GdiElement currentElement, int parentZOrder) {
            if (currentElement == null) {
                return;
            }
            if (currentElement is GdiActiveElement) {
                var el = (GdiActiveElement) currentElement;
                el.GroupZOrder = el.ZOrder + parentZOrder;
                _activeElements.Add(el);
            }
            if (currentElement is GdiGroup) {
                var group = (GdiGroup) currentElement;
                group.childs.ForEach(i => addActiveElements(i, parentZOrder + group.ZOrder));
            }
        }

        public void fitSize() {
            if (RenderElement == null) {
                return;
            }
            var elementSize = _canvasGroup.RealBound.Size;
            if (elementSize.Width.Equals(0) ||
                elementSize.Height.Equals(0)) {
                return;
            }
            int sizeWidth = Size.Width - 2 * OffsetX;
            int sizeHeight = Size.Height - 2 * OffsetY;
            float kx = sizeWidth / elementSize.Width;
            float ky = sizeHeight / elementSize.Height;
            _canvasGroup.Location = new PointF(0, 0);
            _zoomFactor = Math.Min(kx, ky);
            _translation = new PointF((OffsetX + 0.5f * sizeWidth) / _zoomFactor - 0.5f * elementSize.Width,
                (OffsetY + 0.5f * sizeHeight) / _zoomFactor - 0.5f * elementSize.Height);
            Refresh();
        }

        public void increaseZoom() {
            gdiRenderCtrlMouseWheel(this, new MouseEventArgs(MouseButtons.Middle, 1, Width / 2, Height / 2, 120));
        }

        public void decreaseZoom() {
            gdiRenderCtrlMouseWheel(this, new MouseEventArgs(MouseButtons.Middle, 1, Width / 2, Height / 2, -120));
        }

        #endregion

        #region Обработчики событий

        private void gdiRenderCtrlPaint(object sender, PaintEventArgs e) {
            if (RenderElement == null) {
                return;
            }
            e.Graphics.ScaleTransform(_zoomFactor, _zoomFactor);
            e.Graphics.TranslateTransform(_translation.X, _translation.Y);
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            var libObject = new LibGraphicsObject(e.Graphics, _zoomFactor);
            libObject.setRenderObject(_canvasGroup);
            libObject.render();
        }

        protected virtual void gdiRenderCtrlMouseClick(object sender, MouseEventArgs e) {
            bool needRefresh = false;
            foreach (
                var activeElement in
                _activeElements.Where(
                        activeElement => activeElement.Enabled && activeElement.isScreenCoordinatesOnElement(e.X, e.Y))
                    .OrderByDescending(activeElement => activeElement.GroupZOrder)) {
                if (activeElement.raiseMouseClick(e)) {
                    needRefresh = true;
                    break;
                }
            }
            if (needRefresh) {
                Refresh();
            }
        }

        protected virtual void gdiRenderCtrlMouseDoubleClick(object sender, MouseEventArgs e) {
            bool needRefresh = false;
            foreach (
                var activeElement in
                _activeElements.Where(
                    activeElement => activeElement.Enabled && activeElement.isScreenCoordinatesOnElement(e.X, e.Y))) {
                if (activeElement.raiseMouseDoubleClick(e)) {
                    needRefresh = true;
                    break;
                }
            }
            if (needRefresh) {
                Refresh();
            }
        }

        protected virtual void gdiRenderCtrlMouseDown(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Right && AllowMouseTranslate) {
                _translateProcess = true;
                _startTranslationPoint = new Point(e.X, e.Y);
                _startTranslation = _translation;
            }

            bool needRefresh = false;
            foreach (
                var activeElement in
                _activeElements.Where(
                    activeElement => activeElement.Enabled && activeElement.isScreenCoordinatesOnElement(e.X, e.Y))) {
                _translateProcess = false;
                if (activeElement.raiseMouseDown(e)) {
                    needRefresh = true;
                    break;
                }
            }
            if (needRefresh) {
                Refresh();
            }
            if (_translateProcess) {
                Cursor = Cursors.SizeAll;
            }
        }

        protected virtual void gdiRenderCtrlMouseUp(object sender, MouseEventArgs e) {
            if (_translateProcess) {
                _translateProcess = false;
                Cursor = Cursors.Default;
            }

            bool needRefresh = false;
            foreach (
                var activeElement in
                _activeElements.Where(
                    activeElement => activeElement.Enabled && activeElement.isScreenCoordinatesOnElement(e.X, e.Y))) {
                if (activeElement.raiseMouseUp(e)) {
                    needRefresh = true;
                    break;
                }
            }
            if (needRefresh) {
                Refresh();
            }
        }

        protected virtual void gdiRenderCtrlMouseMove(object sender, MouseEventArgs e) {
            bool needRefresh = false;
            if (_translateProcess) {
                var dlt = new Point(e.X - _startTranslationPoint.X, e.Y - _startTranslationPoint.Y);
                _translation.X = _startTranslation.X + dlt.X / _zoomFactor;
                _translation.Y = _startTranslation.Y + dlt.Y / _zoomFactor;
                ViewPortChanged?.Invoke(this, EventArgs.Empty);
                needRefresh = true;
            }
            bool anyHovered = false;
//            bool firstHovered = false;
            foreach (var activeElement in _activeElements) {
                if (activeElement.Enabled) {
                    bool hovered = /*!firstHovered && */ activeElement.isScreenCoordinatesOnElement(e.X, e.Y);
                    if (hovered && activeElement.Enabled) {
                        anyHovered = true;
                    }
                    if (activeElement.processMouseHovering(e, hovered /*, ref firstHovered*/)) {
                        needRefresh = true;
                    }
                }
            }
            if (needRefresh) {
                Refresh();
            }
            if (anyHovered != _activeHovered) {
                _activeHovered = anyHovered;
                Cursor = anyHovered ? Cursors.Hand : Cursors.Default;
            }
        }

        /// <summary>
        /// Scale
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void gdiRenderCtrlMouseWheel(object sender, MouseEventArgs e) {
            var oldZoom = _zoomFactor;

            if (e.Delta > 0) {
                //wheel up
                if (_zoomFactor > ZOOM_FACTOR_MAX) {
                    return;
                }
                _zoomFactor *= ZOOM_FACTOR_KOEFF;
            }
            else {
                //wheel down
                if (_zoomFactor < ZOOM_FACTOR_MIN) {
                    return;
                }
                _zoomFactor /= ZOOM_FACTOR_KOEFF;
            }

            var zoomKoef = (_zoomFactor - oldZoom) / oldZoom / _zoomFactor;
            _translation.X -= e.X * zoomKoef;
            _translation.Y -= e.Y * zoomKoef;

            ViewPortChanged?.Invoke(this, EventArgs.Empty);
            Refresh();
        }

        protected virtual void gdiRenderCtrlResize(object sender, EventArgs e) {
            ViewPortChanged?.Invoke(this, EventArgs.Empty);
        }

        #endregion
    }
}