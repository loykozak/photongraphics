﻿using System.Drawing;

namespace DistributionNetwork.styles {
    /// <summary>
    /// Стиль фигуры
    /// </summary>
    public class ShapeStyle {
        /// <summary>
        /// Активна ли фигура (события мыши)
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Цвет фона неактивной фигуры
        /// </summary>
        public Color InactiveColor { get; set; }

        /// <summary>
        /// Цвет фона фигуры после нажатия
        /// </summary>
        public Color ActiveColor { get; set; }

        /// <summary>
        /// Цвет фона фигуры под курсором
        /// </summary>
        public Color HoveredColor { get; set; }

        /// <summary>
        /// Цвет текста
        /// </summary>
        public Color TextColor { get; set; }

        /// <summary>
        /// Размер текста
        /// </summary>
        public float TextSize { get; set; }

        /// <summary>
        /// Отображать ли границу фигуры
        /// </summary>
        public bool ShowBorder { get; set; }
    }
}