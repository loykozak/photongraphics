﻿using DistributionNetwork.gdi.primitives;

namespace DistributionNetwork.styles {
    /// <summary>
    /// Стиль прямоугольника со скругленными краями
    /// </summary>
    public class RoundedRectangleStyle : RectangleStyle {
        /// <summary>
        /// Радиус скругления
        /// </summary>
        public float Radius { get; set; }
    }


    /// <summary>
    /// Стиль блоба с информацией
    /// </summary>
    public class ParamsBlobStyle : RoundedRectangleStyle {
        /// <summary>
        /// ширина основания стрелики 
        /// </summary>
        public float ArrowWidth { get; set; }
        /// <summary>
        /// высота стрелки
        /// </summary>
        public float ArrowHeight { get; set; }
        /// <summary>
        /// отступ угловых стенок от края (после завершения скругления)
        /// </summary>
        public float ArrowCornerOffset { get; set; }
    }
}