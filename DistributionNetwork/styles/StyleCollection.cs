﻿using System.Drawing;
using DistributionNetwork.gdi.primitives;

namespace DistributionNetwork.styles {
    public static class StyleCollection {

        #region Константы

        /// <summary>
        /// Шрифт
        /// </summary>
        public const string FONT_NAME = "Segoe UI";

        /// <summary>
        /// Минимальное расстояние между элементами (скважинами по горизонтали, кустами по вертикали)
        /// </summary>
        public const float ELEMENTS_SPACING = 5f;

        /// <summary>
        /// Расстояние до инфо баллона
        /// </summary>
        public const float BLOB_DISTANCE = 2f;

        /// <summary>
        /// Длина вертикального соединения от УКПГ до коллектора, от коллектора до сепаратора
        /// </summary>
        public const float COLLECTOR_VERTICAL_DISTANCE = 85f;

        /// <summary>
        /// 
        /// </summary>
        public const float COLLECTOR_VERTICAL_OFFSET = 30f;

        /// <summary>
        /// Длина вертикального соединения до шлейфа
        /// </summary>
        public const float SEPARATOR_VERTICAL_DISTANCE = 90f;

        /// <summary>
        /// Длины соединительных линий для штуцера на мнемосхеме
        /// </summary>
        public const float CHOKE_TOP_CONNECTOR_LENGTH = 65f; //подстроить при изменении количества параметров скважины
        public const float CHOKE_PARAMS_CONNECTOR_LENGTH = 5f;
        public const float CHOKE_BOTTOM_CONNECTOR_LENGTH = 8f;
        public const float CHOKE_ELLIPSE_CONNECTOR_LENGTH = 15f;

        /// <summary>
        /// Размеры стрелочек на мнемосхеме
        /// </summary>
        public const float ARROW_CAP_WIDTH = 6f;
        public const float ARROW_CAP_HEIGHT = 8f;
        public const float ARROW_CAP_INSET = 1.5f;

        /// <summary>
        /// Размеры элементов для штуцера на мнемосхеме
        /// </summary>
        private const float CHOKE_ELLIPSE_SIZE = 30f;
        private const float CHOKE_ELLIPSE_TEXT_SIZE = 6f;
        private const float CHOKE_ROUNDED_RECT_TEXT_SIZE = 7f;
        private const float CHOKE_ROUNDED_RECT_HEIGHT = 45f;
        private const float CHOKE_ROUNDED_RECT_WIDTH = 89f;

        /// <summary>
        /// Размеры элементов для куста на мнемосхеме
        /// </summary>
        public const float RIG_ELLIPSE_SIZE = 40f;
        private const float RIG_ELLIPSE_TEXT_SIZE = 8f;
        private const float RIG_ROUNDED_RECT_TEXT_SIZE = 7f;
        private const float RIG_ROUNDED_RECT_HEIGHT = 31f;
        private const float RIG_ROUNDED_RECT_WIDTH = 73f;
        public const float RIG_ROUNDED_PARAMS_LENGTH = 9f;

        /// <summary>
        /// Размеры элементов для шлейфа на мнемосхеме
        /// </summary>
        private const float SEPARATOR_ELLIPSE_SIZE = 40f;
        private const float SEPARATOR_ELLIPSE_TEXT_SIZE = 9f;
        private const float SEPARATOR_ROUNDED_RECT_TEXT_SIZE = 7f;
        private const float SEPARATOR_ROUNDED_RECT_HEIGHT = 31f;
        private const float SEPARATOR_ROUNDED_RECT_WIDTH = 73f;
        public const float SEPARATOR_ROUNDED_PARAMS_LENGTH = 10f;
        public const float SEPARATORS_DISTANCE = 90f;

        /// <summary>
        /// Размеры элементов для коллектора на мнемосхеме
        /// </summary>
        private const float GROUP_NAME_WIDTH = 71f;
        private const float GROUP_NAME_HEIGHT = 31f;
        private const float GROUP_NAME_TEXT_SIZE = 9f;
        private const float GROUP_ROUNDED_RECT_TEXT_SIZE = 7f;
        private const float GROUP_ROUNDED_RECT_HEIGHT = 50f;
        private const float GROUP_ROUNDED_RECT_WIDTH = 94f;
        public const float GROUP_ROUNDED_PARAMS_LENGTH = 10f;

        /// <summary>
        /// Размеры прямоугольника с именем скважины на мнемосхеме
        /// </summary>
        private const float WELL_NAME_RECT_HEIGHT = 15f;
        public const float WELL_NAME_RECT_WIDTH = 40f;
        private const float WELL_NAME_TEXT_SIZE = 8f;

        /// <summary>
        /// Размер штуцера на мнемосхеме
        /// </summary>
        private const float CHOKE_HEIGHT = 20f;
        private const float CHOKE_WIDTH = 10f;

        /// <summary>
        /// Толщина линий на мнемосхеме
        /// </summary>
        public const float LINE_WIDTH = 2f;

        /// <summary>
        /// Толщина линий для соединения с параметрами на мнемосхеме
        /// </summary>
        public const float PARAMS_LINE_WIDTH = 1f;

        /// <summary>
        /// Радиус скругления прямоугольников
        /// </summary>
        public const float ROUNED_RECT_RADIUS = 5f;

        /// <summary>
        /// Радиус для элемента пересечения линий
        /// </summary>
        public const float ROUNED_CROSS_RADIUS = 5f;

        #endregion

        #region Цвета

        /// <summary>
        /// Цвет фона
        /// </summary>
        public static readonly Color backgroundColor = Color.White;

        /// <summary>
        /// Цвета линий для коллекторов
        /// </summary>
        public static readonly Color[] collectorLineColors = new[]
        {
            Color.FromArgb(88, 164, 133),
            Color.FromArgb(96, 129, 167),
            Color.Maroon,
            Color.DarkBlue
        };

        /// <summary>
        /// Цвет штуцера
        /// </summary>
        public static readonly Color chokeColor = Color.FromArgb(94, 128, 166);

        /// <summary>
        /// Цвет текста на фигурах
        /// </summary>
        private static readonly Color ShapeTextColor = Color.White;

        /// <summary>
        /// Цвет неактивной фигуры
        /// </summary>
        public static readonly Color DisabledColor = Color.FromArgb(202, 202, 202);

        /// <summary>
        /// Цвет неисправного элемента
        /// </summary>
        public static readonly Color AlarmColor = Color.FromArgb(218, 88, 89);

        /// <summary>
        /// Цвет активного элемента при наведении
        /// </summary>
        private static readonly Color HoverColor = Color.FromArgb(99, 199, 143);

        /// <summary>
        /// Цвет активного элемента при нажатии
        /// </summary>
        private static readonly Color ActiveColor = Color.FromArgb(22, 163, 65);

        #endregion


        #region Стили фигур

        /// <summary>
        /// Стиль прямоугольника группы (УКПГ)
        /// </summary>
        public static readonly RectangleStyle groupNameStyle = new RectangleStyle
        {
            IsActive = true,
            InactiveColor = Color.FromArgb(76, 108, 140),
            HoveredColor = HoverColor,
            ActiveColor = ActiveColor,
            TextColor = ShapeTextColor,
            TextSize = GROUP_NAME_TEXT_SIZE,
            ShowBorder = false,
            Width = GROUP_NAME_WIDTH,
            Height = GROUP_NAME_HEIGHT
        };

        /// <summary>
        /// Стиль эллипса для коллектора
        /// </summary>
        public static readonly EllipseStyle collectorEllipseStyle = new EllipseStyle
        {
            IsActive = true,
            InactiveColor = Color.FromArgb(187, 115, 62),
            HoveredColor = HoverColor,
            ActiveColor = ActiveColor,
            TextColor = ShapeTextColor,
            TextSize = RIG_ELLIPSE_TEXT_SIZE,
            ShowBorder = false,
            Size = CHOKE_ELLIPSE_SIZE
        };

        /// <summary>
        /// Стиль эллипса сепаратора (шлейфа)
        /// </summary>
        public static readonly EllipseStyle separatorEllipseStyle = new EllipseStyle {
            IsActive = true,
            InactiveColor = Color.FromArgb(187, 115, 62),
            HoveredColor = HoverColor,
            ActiveColor = ActiveColor,
            TextColor = ShapeTextColor,
            TextSize = SEPARATOR_ELLIPSE_TEXT_SIZE,
            ShowBorder = false,
            Size = SEPARATOR_ELLIPSE_SIZE
        };

        /// <summary>
        /// Стиль эллипса куста
        /// </summary>
        public static readonly EllipseStyle rigEllipseStyle = new EllipseStyle {
            IsActive = true,
            InactiveColor = Color.FromArgb(242, 167, 75),
            HoveredColor = HoverColor,
            ActiveColor = ActiveColor,
            TextColor = ShapeTextColor,
            TextSize = RIG_ELLIPSE_TEXT_SIZE,
            ShowBorder = false,
            Size = RIG_ELLIPSE_SIZE
        };

        /// <summary>
        /// Стиль эллипса скважины
        /// </summary>
        public static readonly EllipseStyle chokeEllipseStyle = new EllipseStyle {
            InactiveColor = Color.FromArgb(155, 176, 200),
            TextColor = ShapeTextColor,
            TextSize = CHOKE_ELLIPSE_TEXT_SIZE,
            ShowBorder = false,
            Size = CHOKE_ELLIPSE_SIZE
        };

        /// <summary>
        /// Стиль прямоугольника с наименованием скважины
        /// </summary>
        public static readonly RectangleStyle itemNameRectangleStyle = new RectangleStyle
        {
            IsActive = true,
            InactiveColor = Color.FromArgb(104, 206, 232),
            HoveredColor = HoverColor,
            ActiveColor = ActiveColor,
            TextColor = ShapeTextColor,
            TextSize = WELL_NAME_TEXT_SIZE,
            ShowBorder = false,
            Height = WELL_NAME_RECT_HEIGHT,
            Width = WELL_NAME_RECT_WIDTH
        };

        /// <summary>
        /// Стиль штуцера
        /// </summary>
        public static readonly RectangleStyle chokeStyle = new RectangleStyle {
            IsActive = true,
            InactiveColor = chokeColor,
            HoveredColor = HoverColor,
            ActiveColor = ActiveColor,
            ShowBorder = true,
            Height = CHOKE_HEIGHT,
            Width = CHOKE_WIDTH
        };

        /// <summary>
        /// Стиль прямоугольника с параметрами скважины
        /// </summary>
        public static readonly ParamsBlobStyle wellParamsStyle = new ParamsBlobStyle
        {
            InactiveColor = Color.FromArgb(155, 176, 200),
            TextColor = ShapeTextColor,
            TextSize = CHOKE_ROUNDED_RECT_TEXT_SIZE,
            ShowBorder = false,
            Radius = ROUNED_RECT_RADIUS,
            Height = CHOKE_ROUNDED_RECT_HEIGHT,
            Width = CHOKE_ROUNDED_RECT_WIDTH,
            ArrowHeight = 15,
            ArrowWidth = 10,
            ArrowCornerOffset = 5
        };

        /// <summary>
        /// Стиль прямоугольника с параметрами куста
        /// </summary>
        public static readonly ParamsBlobStyle rigParamsStyle = new ParamsBlobStyle
        {
            InactiveColor = Color.FromArgb(155, 176, 200),
            TextColor = ShapeTextColor,
            TextSize = RIG_ROUNDED_RECT_TEXT_SIZE,
            ShowBorder = false,
            Radius = ROUNED_RECT_RADIUS,
            Height = RIG_ROUNDED_RECT_HEIGHT,
            Width = RIG_ROUNDED_RECT_WIDTH,
            ArrowHeight = 10,
            ArrowWidth = 10,
            ArrowCornerOffset = 5
        };

        /// <summary>
        /// Стиль прямоугольника с параметрами сепаратора
        /// </summary>
        public static readonly ParamsBlobStyle separatorParamsStyle = new ParamsBlobStyle
        {
            InactiveColor = Color.FromArgb(155, 176, 200),
            TextColor = ShapeTextColor,
            TextSize = SEPARATOR_ROUNDED_RECT_TEXT_SIZE,
            ShowBorder = false,
            Radius = ROUNED_RECT_RADIUS,
            Height = SEPARATOR_ROUNDED_RECT_HEIGHT,
            Width = SEPARATOR_ROUNDED_RECT_WIDTH,
            ArrowHeight = 10,
            ArrowWidth = 10,
            ArrowCornerOffset = 5
        };

        /// <summary>
        /// Стиль прямоугольника с параметрами группы (коллектора)
        /// </summary>
        public static readonly ParamsBlobStyle groupParamsStyle = new ParamsBlobStyle
        {
            InactiveColor = Color.FromArgb(155, 176, 200),
            TextColor = ShapeTextColor,
            TextSize = GROUP_ROUNDED_RECT_TEXT_SIZE,
            ShowBorder = false,
            Radius = ROUNED_RECT_RADIUS,
            Height = GROUP_ROUNDED_RECT_HEIGHT,
            Width = GROUP_ROUNDED_RECT_WIDTH,
            ArrowHeight = 10,
            ArrowWidth = 10,
            ArrowCornerOffset = 5
        };

        /// <summary>
        /// Стиль прямоугольника с параметрами группы (коллектора)
        /// </summary>
        public static readonly ParamsBlobStyle groupBlobParamsStyle = new ParamsBlobStyle {
            InactiveColor = Color.FromArgb(155, 176, 200),
            TextColor = ShapeTextColor,
            TextSize = GROUP_ROUNDED_RECT_TEXT_SIZE,
            ShowBorder = false,
            Radius = ROUNED_RECT_RADIUS,
            Height = GROUP_ROUNDED_RECT_HEIGHT,
            Width = GROUP_ROUNDED_RECT_WIDTH,
            ArrowHeight = 10,
            ArrowWidth = 10,
            ArrowCornerOffset = 5
        };


        #endregion

    }
}
