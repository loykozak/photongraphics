﻿namespace DistributionNetwork.styles {
    /// <summary>
    /// Стиль окружности
    /// </summary>
    public class EllipseStyle : ShapeStyle {
        /// <summary>
        /// Размер (диаметр)
        /// </summary>
        public float Size { get; set; }
    }
}