﻿namespace DistributionNetwork.styles {
    /// <summary>
    /// Стиль прямоугольника
    /// </summary>
    public class RectangleStyle : ShapeStyle {
        /// <summary>
        /// Высота
        /// </summary>
        public float Height { get; set; }

        /// <summary>
        /// Ширина
        /// </summary>
        public float Width { get; set; }
    }
}