﻿using System;
using System.Drawing;
using System.Windows.Forms;
using PhotonGraphics.elements.baseElements;
using PhotonGraphics.elements.shapes;

namespace DistributionNetwork.gdi {
    /// <summary>
    /// Графический элемент - группа кнопок для управления масштабом
    /// </summary>
    public class ScaleButtons : GdiGroup {
        #region Стиль элемента

        private const float BUTTON_HEIGHT = 35f;
        private const float BUTTON_DISTANCE = 10f;

        private const float TEXT_SCALE_SIZE = 10f;
        private const float TEXT_PLUS_SIZE = 30f;
        private const float TEXT_MINUS_SIZE = 50f;
        private const int TEXT_PADDING = 5;

        private static readonly Color TextColor = Color.White;
        private static readonly Color InactiveColor = Color.FromArgb(59, 158, 235);
        private static readonly Color HoveredColor = Color.FromArgb(20, 137, 227);
        private static readonly Color ActiveColor = Color.FromArgb(6, 118, 204);

        #endregion

        public event EventHandler OnPlusButtonClick;
        public event EventHandler OnMinusButtonClick;
        public event EventHandler OnFullSizeButtonClick;

        public ScaleButtons() {
            IsConstantElement = true;
            ZOrder = 1000;

            var btnMinus = createButtonRect(TEXT_MINUS_SIZE);
            btnMinus.Text = "-";
            btnMinus.TextPadding = new Padding(-10, -8, -10, 0);
            childs.Add(btnMinus);

            var btnFullSize = createButtonRect(TEXT_SCALE_SIZE);
            btnFullSize.Text = "100%";
            btnFullSize.adjustSize();
            btnFullSize.Size = new SizeF(btnFullSize.Size.Width, BUTTON_HEIGHT);
            childs.Add(btnFullSize);

            var btnPlus = createButtonRect(TEXT_PLUS_SIZE);
            btnPlus.Text = "+";
            btnPlus.TextPadding = new Padding(1, -2, -1, 2);
            childs.Add(btnPlus);

            btnMinus.Center = new PointF();
            btnFullSize.Center = new PointF(btnMinus.Size.Width/2 + BUTTON_DISTANCE + btnFullSize.Size.Width/2, 0);
            btnPlus.Center =
                new PointF(btnMinus.Size.Width/2 + 2*BUTTON_DISTANCE + btnFullSize.Size.Width + btnPlus.Size.Width/2, 0);


            btnMinus.MouseClick += (_, __) => {
                var h = OnMinusButtonClick;
                if (h != null) {
                    h(this, EventArgs.Empty);
                }
            };
            btnPlus.MouseClick += (_, __) => {
                var h = OnPlusButtonClick;
                if (h != null) {
                    h(this, EventArgs.Empty);
                }
            };
            btnFullSize.MouseClick += (_, __) => {
                var h = OnFullSizeButtonClick;
                if (h != null) {
                    h(this, EventArgs.Empty);
                }
            };
        }

        private static GdiRectangle createButtonRect(float textSize) {
            var rect = new GdiRectangle
            {
                Size = new SizeF(BUTTON_HEIGHT, BUTTON_HEIGHT),
                DrawBorder = false,
                DrawFill = true,
                ShapeBrush = new SolidBrush(InactiveColor),
                TextBrush = new SolidBrush(TextColor),
                Font = new Font("Segoe UI", textSize),
                TextPadding = new Padding(TEXT_PADDING, 0, TEXT_PADDING, 0)
            };
            rect.MouseHover += (_, __) => rect.ShapeBrush = new SolidBrush(HoveredColor);
            rect.MouseLeave += (_, __) => rect.ShapeBrush = new SolidBrush(InactiveColor);
            rect.MouseDown += (_, __) => rect.ShapeBrush = new SolidBrush(ActiveColor);
            rect.MouseUp += (_, __) => rect.ShapeBrush = new SolidBrush(HoveredColor);
            return rect;
        }
    }
}