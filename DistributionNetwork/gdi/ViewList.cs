﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DistributionNetwork.Properties;
using PhotonGraphics.elements.baseElements;
using PhotonGraphics.elements.shapes;

namespace DistributionNetwork.gdi
{
    /// <summary>
    /// Графический элемент списка объектов отображения
    /// </summary>
    public class ViewList : GdiGroup {

        #region Стиль элемента

        /// <summary>
        /// Ширина элемента
        /// </summary>
        public const float LIST_WIDTH = 220f;

        /// <summary>
        /// Цвет фона
        /// </summary>
        private static readonly Color BackgroundColor = Color.Transparent;//Color.CadetBlue;

        /// <summary>
        /// Отступы
        /// </summary>
        public static Padding listPadding = new Padding(5);

        /// <summary>
        /// Вертикальное расстояние между элементами
        /// </summary>
        private const float LIST_VERTICAL_SPACING = 3f;

        /// <summary>
        /// Размер текста заголовка
        /// </summary>
        private const float TITLE_TEXT_SIZE = 12f;

        /// <summary>
        /// Цвет текста заголовка
        /// </summary>
        private static readonly Color TitleTextColor = Color.FromArgb(98, 126, 154);

        /// <summary>
        /// Размер иконки
        /// </summary>
        public const float ITEM_IMAGE_SIZE = 10f;

        /// <summary>
        /// Размер текста элемента
        /// </summary>
        public const float ITEM_TEXT_SIZE = 10f;

        /// <summary>
        /// Цвет текста элемента
        /// </summary>
        public static Color itemTextColor = Color.FromArgb(95, 127, 169);

        /// <summary>
        /// Расстояние от иконки до текста в элементе
        /// </summary>
        public const float ITEM_ICON_DISTANCE = 10f;

        #endregion

        #region Поля

        private string _title;
        private List<VievListItem> _items;

        #endregion


        #region Свойства

        //Заголовок списка
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                updateGraphics();
            }
        }

        //Список элементов
        public List<VievListItem> Items
        {
            get { return _items; }
            set
            {
                _items = value;
                updateGraphics();
            }
        }

        #endregion

        public ViewList() {
            Items = new List<VievListItem>();
            IsConstantElement = true;
            ZOrder = 1000;
        }

        public void updateGraphics() {
            childs.Clear();

            var background = new GdiRectangle
            {
                Enabled = false,
                DrawBorder = false,
                DrawFill = true,
                ShapeBrush = new SolidBrush(BackgroundColor)
            };
            childs.Add(background);

            float verticalPosition = listPadding.Top;
            if (!string.IsNullOrWhiteSpace(Title)) {
                var title = new GdiTextBlock
                {
                    Enabled = false,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Text = Title,
                    Font = new Font("Segoe UI", TITLE_TEXT_SIZE),
                    TextBrush = new SolidBrush(TitleTextColor),
                    Location = new PointF(listPadding.Left, verticalPosition)
                };
                var textWidth = LIST_WIDTH - listPadding.Left - listPadding.Right;
                title.Size = new SizeF(textWidth, title.calcTextSize(new SizeF(textWidth, 1000)).Height);
                verticalPosition += title.Size.Height;
                childs.Add(title);
            }
            if (Items != null) {
                var gdiItems = Items.Select(i => new VievListGdiItem(i)).ToList();
                foreach (var gdiItem in gdiItems) {
                    verticalPosition += LIST_VERTICAL_SPACING;
                    gdiItem.Location = new PointF(listPadding.Left, verticalPosition);
                    verticalPosition += gdiItem.Size.Height;
                }
                childs.AddRange(gdiItems);
            }
            background.Size = new SizeF(LIST_WIDTH, verticalPosition + listPadding.Bottom);
        }
    }

    /// <summary>
    /// Графический элемент списка
    /// </summary>
    public class VievListGdiItem : GdiGroup {
        private static readonly Image CheckedImage = Resources.graph_active;
        private static readonly Image UncheckedImage = Resources.graph;

        public VievListGdiItem(VievListItem source) {

            var icon = new GdiImage
            {
                Enabled = true,
                Image = source.IsChecked ? CheckedImage : UncheckedImage,
                Size = new SizeF(ViewList.ITEM_IMAGE_SIZE, ViewList.ITEM_IMAGE_SIZE),
                SizeMode = PictureBoxSizeMode.Zoom,
                Center = new PointF(0, 0),
                Tag = source
            };
            icon.MouseClick += onIconClick;
            childs.Add(icon);

            var textWidth = ViewList.LIST_WIDTH - ViewList.listPadding.Left - ViewList.listPadding.Right -
                            ViewList.ITEM_ICON_DISTANCE - icon.Size.Width;
            var text = new GdiTextBlock
            {
                Enabled = false,
                HorizontalAlignment = HorizontalAlignment.Left,
                Text = source.Text,
                Font = new Font("Segoi UI", ViewList.ITEM_TEXT_SIZE),
                TextBrush = new SolidBrush(ViewList.itemTextColor)
            };
            text.Size = new SizeF(textWidth, text.calcTextSize(new SizeF(textWidth, 1000)).Height);
            text.Center = new PointF(icon.Size.Width/2 + ViewList.ITEM_ICON_DISTANCE + text.Size.Width/2, 0);
            childs.Add(text);
        }

        private void onIconClick(object sender, MouseEventArgs e) {
            var icon = sender as GdiImage;
            if (icon == null) {
                return;
            }
            var source = icon.Tag as VievListItem;
            if (source == null) {
                return;
            }
            source.IsChecked = !source.IsChecked;
            icon.Image = source.IsChecked ? CheckedImage : UncheckedImage;
            source.raiseOnCheckStateChanged();
        }
    }

    /// <summary>
    /// Элемент списка для отображения
    /// </summary>
    public class VievListItem {

        /// <summary>
        /// Подпись элемента
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Состояние элемента
        /// </summary>
        public bool IsChecked { get; set; }

        /// <summary>
        /// Тэг
        /// </summary>
        public object Tag { get; set; }

        public event EventHandler OnCheckStateChanged;

        public void raiseOnCheckStateChanged() {
            var h = OnCheckStateChanged;
            if (h != null) {
                h(this, EventArgs.Empty);
            }
        }
    }
}
