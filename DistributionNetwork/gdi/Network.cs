﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using DistributionNetwork.dto;
using DistributionNetwork.gdi.primitives;
using DistributionNetwork.styles;
using PhotonGraphics.elements.baseElements;
using PhotonGraphics.elements.shapes;

namespace DistributionNetwork.gdi {
    public class Network : GdiGroup {
        public readonly NetworkDto source;
        private readonly NameRectangle _infoRectangle;
        private readonly Separator[] _sepList;

        public Network(NetworkDto source) {
            this.source = source;

            //Прямоугольник с именем коллектора
            _infoRectangle = new NameRectangle(this.source, StyleCollection.groupNameStyle)
            {
                Tag = this.source,
                Center = new PointF(0, 0)
            };

            //Прямоугольник с параметрами
            var paramsRect = new ParamsBlob(this.source, StyleCollection.groupParamsStyle)
            {
                Direct = ParamsBoubleDirect.Up,
                BlobAnchorPoint = new PointF(0,
                    _infoRectangle.Size.Height/2 + StyleCollection.BLOB_DISTANCE)
            };

            //Создание шлейфов
            var separators = source.Collectors.SelectMany(i => i.Separators).ToList();
            //Группировка по именам шлейфов
            separators = separators
                .OrderBy(i => i.Name)
                .GroupBy(i => i.Name)
                .SelectMany(g => g.OrderBy(i => i.Collector.OrderNumber).ToList())
                .ToList();

            //Группы сепараторов
            int sepCount = separators.Count;
            int sepCount2 = sepCount/2;
            var separatorGroups = separators
                .OrderBy(i => i.Name)
                .Select((i, index) => new {Separator = i, Index = index})
                .GroupBy(i => i.Separator.Name)
                .Select(
                    g => {
                        var connectionsOnLeftList = g.Select(i => i.Index < sepCount2).ToList();
                        bool connectionsOnLeft = connectionsOnLeftList.Count(i => i == true) > g.Count()/2;
                        return new SeparatorGroup(connectionsOnLeft,
                            g.OrderBy(i => i.Separator.Collector.OrderNumber)
                                .Select(pair => new KeyValuePair<int, SeparatorDto>(pair.Index, pair.Separator))
                                .ToList());
                    })
                .ToList();

            //Вертикальная позиция сепараторов
            var separatorLocationY = -StyleCollection.COLLECTOR_VERTICAL_DISTANCE*source.Collectors.Count -
                                     StyleCollection.COLLECTOR_VERTICAL_OFFSET;

                _sepList = separatorGroups.SelectMany(i => i).ToArray();

                //признак четности
                bool isEven = sepCount%2 == 0;

                //Расстановка шлейфов

                //Левая часть
                for (int i = 0; i < sepCount2; i++) {
                    _sepList[i].SeparatorAnchorPoint =
                        new PointF(
                            (isEven ? i - sepCount2 + 0.5f : i - sepCount2)*
                            StyleCollection.SEPARATORS_DISTANCE,
                            separatorLocationY);
                }
                //Правая часть
                for (int i = sepCount - 1; i >= sepCount2; i--) {
                    _sepList[i].SeparatorAnchorPoint =
                        new PointF(
                            (isEven ? i - sepCount2 + 0.5f : i - sepCount2)*
                            StyleCollection.SEPARATORS_DISTANCE,
                            separatorLocationY);
                }

                #region соединение сепараторов с УКПГ

                //расстановка коллекторов
                var collectorGdiList = source.Collectors.Select(i => new Collector(i)).ToList();
                var leftXcoord = (isEven ? -sepCount2 + 1 : -sepCount2 + 0.5f)*StyleCollection.SEPARATORS_DISTANCE;
                int collSpace = separators.Count/collectorGdiList.Count;
                int collOffset = collSpace/2;
                if (collectorGdiList.Count > 1) {
                    for (int i = 0; i < collectorGdiList.Count; i++) {
                        collectorGdiList[i].CollectorAnchorPoint =
                            new PointF(leftXcoord + (collOffset + i*collSpace)*StyleCollection.SEPARATORS_DISTANCE,
                                -StyleCollection.COLLECTOR_VERTICAL_DISTANCE*(i + 1));
                    }
                } else {
                    collectorGdiList[0].CollectorAnchorPoint =
                        new PointF(0, -StyleCollection.COLLECTOR_VERTICAL_DISTANCE);
                }
                foreach (var collector in collectorGdiList) {
                    collector.Separators =
                        _sepList.Where(sep => sep.source.Collector.Name.Equals(collector.source.Name)).ToList();
                }

                //соединение коллекторов с УКПГ
                var ctguConnections = new List<GdiPolyline>();
                ctguConnections.AddRange(collectorGdiList.Select(collectorGdi => {
                    var collectorPoint = collectorGdi.CollectorAnchorPoint;
                    var verticalLine = GdiObjectFactory.createPolyLine(collectorGdi.source.OrderNumber);
                    verticalLine.Points.AddRange(new[]
                    {
                        new PointF(collectorPoint.X, collectorPoint.Y),
                        new PointF(collectorPoint.X, 0)
                    });
                    return verticalLine;
                }));
                if (ctguConnections.Count > 1) {
                    var horizontalLine = GdiObjectFactory.createPolyLine();
                    var collectorLeftX = Math.Min(collectorGdiList.Min(sep => sep.CollectorAnchorPoint.X), 0);
                    var collectorRightX = Math.Max(collectorGdiList.Max(sep => sep.CollectorAnchorPoint.X), 0);
                    horizontalLine.Points.AddRange(new[]
                    {
                        new PointF(collectorLeftX, 0),
                        new PointF(collectorRightX, 0)
                    });
                    ctguConnections.Add(horizontalLine);
                }

                //соединение сепараторов с коллекторами
                var collectorVerticalConnections = _sepList
                    .Select(separatorGdi => {
                        var collectorGdi =
                            collectorGdiList.First(coll => coll.source.Name.Equals(separatorGdi.source.Collector.Name));
                        var collectorPoint = collectorGdi.CollectorAnchorPoint;
                        var separatorPoint = separatorGdi.SeparatorAnchorPoint;
                        var verticalLine = GdiObjectFactory.createPolyLine(collectorGdi.source.OrderNumber);
                        verticalLine.Points.AddRange(new[]
                        {
                            new PointF(separatorPoint.X, separatorPoint.Y),
                            new PointF(separatorPoint.X, collectorPoint.Y)
                        });
                        return verticalLine;
                    })
                    .ToList();

                var allVerticalConnectors = collectorVerticalConnections.Concat(ctguConnections).ToList();

                var collectorHorizontalConnections = collectorGdiList
                    .Select(collectorGdi => {
                        var separatorGdiList =
                            _sepList.Where(sep => sep.source.Collector.Name.Equals(collectorGdi.source.Name)).ToList();
                        var collectorPoint = collectorGdi.CollectorAnchorPoint;
                        var separatorLeftX = Math.Min(separatorGdiList.Min(sep => sep.SeparatorAnchorPoint.X),
                            collectorPoint.X);
                        var separatorRightX = Math.Max(separatorGdiList.Max(sep => sep.SeparatorAnchorPoint.X),
                            collectorPoint.X);
                        var startLinePoint = new PointF(separatorLeftX, collectorPoint.Y);
                        var endLinePoint = new PointF(separatorRightX, collectorPoint.Y);
                        return new JumpLine(GdiObjectFactory.createLinePen(collectorGdi.source.OrderNumber),
                            startLinePoint,
                            endLinePoint,
                            allVerticalConnectors);
                    })
                    .ToList();

                #endregion

                var leftSepList = _sepList.Where(i => i.ConnectionOnLeft).OrderBy(i => i.PositionIndex).ToList();
                var rightSepList =
                    _sepList.Where(i => !i.ConnectionOnLeft).OrderByDescending(i => i.PositionIndex).ToList();

                childs.AddRange(leftSepList.Concat(rightSepList));
                childs.AddRange(ctguConnections);
                childs.AddRange(collectorVerticalConnections);
                childs.AddRange(collectorHorizontalConnections);
                childs.AddRange(collectorGdiList);

            //Установка длин шлейфов - левая часть
            float sepVerticalOffsetLeft = StyleCollection.SEPARATOR_VERTICAL_DISTANCE;
            var leftSeparatorGroups =
                separatorGroups
                    .Where(i => i.ConnectionOnLeft)
                    .OrderBy(i => i.Max(s => s.PositionIndex))
                    .ToList();
            foreach (var sepGroup in leftSeparatorGroups) {
                sepGroup.GroupVerticalPosition = sepVerticalOffsetLeft;
                sepVerticalOffsetLeft += sepGroup.GroupHeight + StyleCollection.ELEMENTS_SPACING;
                sepGroup.updateGeometry();
            }

            //Установка длин шлейфов - правая часть
            float sepVerticalOffsetRight = StyleCollection.SEPARATOR_VERTICAL_DISTANCE;
            var rightSeparatorGroups =
                separatorGroups
                    .Where(i => !i.ConnectionOnLeft)
                    .OrderByDescending(i => i.Max(s => s.PositionIndex))
                    .ToList();
            foreach (var sepGroup in rightSeparatorGroups) {
                sepGroup.GroupVerticalPosition = sepVerticalOffsetRight;
                sepVerticalOffsetRight += sepGroup.GroupHeight + StyleCollection.ELEMENTS_SPACING;
                sepGroup.updateGeometry();
            }

            //создание и расстановка перемычек на шлейфах
            var separatorJumpers =
                source.Collectors
                    .SelectMany(coll => coll.SeparatorJumpers
                        .Select(jumper => {
                            var sep1Gdi =
                                separatorGroups.SelectMany(i => i.Where(j => j.source == jumper.Separator1)).Single();
                            var sep2Gdi =
                                separatorGroups.SelectMany(i => i.Where(j => j.source == jumper.Separator2)).Single();

                            return new SeparatorJumper(jumper, coll.OrderNumber)
                            {
                                Separator1 = sep1Gdi,
                                Separator2 = sep2Gdi,
                                SectionsCount = Math.Abs(sep1Gdi.PositionIndex - sep2Gdi.PositionIndex)
                            };
                        }))
                    .OrderBy(j => Math.Min(j.Separator1.PipeLineLength, j.Separator2.PipeLineLength))
                    .ToList();

            if (separatorJumpers.Any()) {
                var firstSepJumper = separatorJumpers[0];
                var sepPipeLines = separatorGroups.SelectMany(grp => grp.GroupPipeLineInfos).ToList();
                var startLength = firstSepJumper.Separator1.SeparatorInfoHeight + StyleCollection.SEPARATOR_VERTICAL_DISTANCE;
                foreach (var separatorJumper in separatorJumpers) {
                    var sepPos = Math.Min(separatorJumper.Separator1.SeparatorAnchorPoint.X,
                        separatorJumper.Separator2.SeparatorAnchorPoint.X);
                    separatorJumper.SeparatorJumperAnchorPoint = new PointF(sepPos, separatorLocationY - startLength);
                    startLength += 60; //todo move to styles
                    separatorJumper.updateGeometry(sepPipeLines);
                }
                childs.AddRange(separatorJumpers);
            }

            foreach (var collector in collectorGdiList)
            {
                collector.SeparatorJumpers =
                    separatorJumpers.Where(sep => sep.source.Separator1.Collector.Name.Equals(collector.source.Name)).ToList();
            }

            childs.Add(_infoRectangle);
            childs.Add(paramsRect);
        }
    }
}