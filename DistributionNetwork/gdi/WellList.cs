﻿using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using DistributionNetwork.dto;
using DistributionNetwork.gdi.primitives;
using DistributionNetwork.styles;
using PhotonGraphics.elements.baseElements;

namespace DistributionNetwork.gdi {
    /// <summary>
    /// Группа скважин на мнемосхеме
    /// </summary>
    public class WellList : GdiGroup {
        public readonly List<WellDto> source;
        private readonly Well[] _wellList;

        public int CollectorOrderNumber { get; private set; }

        public bool ConnectionOnLeft { get; set; }

        public PointF WellListAnchorPoint
        {
            get
            {
                if (_wellList.Length == 0) {
                    return new PointF();
                }
                var location = Location;
                var childsLocation = ChildsLocation;
                var size = Size;
                return new PointF(ConnectionOnLeft 
                    ? _wellList.Last().WellAnchorPoint.X - childsLocation.X + location.X
                    : _wellList.First().WellAnchorPoint.X - childsLocation.X + location.X, 
                    location.Y + size.Height);
            }
            set
            {
                if (_wellList.Length == 0)
                {
                    return;
                }
                var childsLocation = ChildsLocation;
                var size = Size;
                Location = new PointF(ConnectionOnLeft 
                    ? value.X - _wellList.Last().WellAnchorPoint.X + childsLocation.X
                    : value.X - _wellList.First().WellAnchorPoint.X + childsLocation.X,
                    value.Y - size.Height);
            }
        }

        public ObjectState ParentState
        {
            set
            {
                foreach (var well in _wellList) {
                    well.ParentState = value;
                }
            }
        }

        public WellList(List<WellDto> source, bool connectionOnLeft, int collectorOrderNumber) {
            this.source = source;
            ConnectionOnLeft = connectionOnLeft;
            CollectorOrderNumber = collectorOrderNumber;

            if (source == null ||
                !source.Any()) {
                return;
            }

            _wellList = this.source.Select(well => new Well(well, ConnectionOnLeft, collectorOrderNumber)).ToArray();

            float posX = 0;
            var lineEndX = posX;
            foreach (Well wellVg in _wellList) {
                wellVg.WellAnchorPoint = new PointF(posX, 0);
                lineEndX = posX;
                posX += wellVg.Size.Width + StyleCollection.ELEMENTS_SPACING;
            }
            if (source.Count > 1) {
                var line = GdiObjectFactory.createPolyLine(collectorOrderNumber);
                line.Points.AddRange(new[]
                {
                    new PointF(0, 0),
                    new PointF(lineEndX, 0)
                });
                childs.Add(line);
            }

            childs.AddRange(_wellList);
        }
    }
}