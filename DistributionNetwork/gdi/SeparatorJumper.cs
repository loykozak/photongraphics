﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using DistributionNetwork.dto;
using DistributionNetwork.gdi.primitives;
using DistributionNetwork.styles;
using PhotonGraphics.elements.baseElements;
using PhotonGraphics.elements.shapes;

namespace DistributionNetwork.gdi {
    public class SeparatorJumper : GdiGroup {
        public readonly SeparatorJumperDto source;

        private readonly JumpLine _line;
        private readonly GdiPolyline _leftVerticalLine;
        private readonly GdiPolyline _rightVerticalLine;
        private readonly WellChoke _choke;
        private readonly ChokeDiamEllipse _chokeDiam;

        private ObjectState _jumperState = ObjectState.osEnabled;
        private ObjectState _parentState = ObjectState.osEnabled;

        public int SectionsCount { get; set; }

        public Separator Separator1 { get; set; }

        public Separator Separator2 { get; set; }

        public PointF SeparatorJumperAnchorPoint { get; set; }

        public ObjectState JumperState
        {
            get { return _jumperState; }
            set
            {
                _jumperState = value;
                if (ParentState == ObjectState.osEnabled) {
                    _choke.State = value;
                    _chokeDiam.State = value;
                } else {
                    if (_jumperState == ObjectState.osAlarm) {
                        _choke.State = ObjectState.osAlarm;
                    } else {
                        _choke.State = ObjectState.osDisabled;
                    }
                    _chokeDiam.State = ObjectState.osDisabled;
                }
            }
        }

        public ObjectState ParentState
        {
            get { return _parentState; }
            set
            {
                _parentState = value;
                JumperState = JumperState;
            }
        }

        public SeparatorJumper(SeparatorJumperDto source, int collectorOrderNumber) {
            this.source = source;

            _line = new JumpLine(GdiObjectFactory.createLinePen(collectorOrderNumber));
            childs.Add(_line);

            _leftVerticalLine = GdiObjectFactory.createPolyLine(collectorOrderNumber);
            childs.Add(_leftVerticalLine);

            _rightVerticalLine = GdiObjectFactory.createPolyLine(collectorOrderNumber);
            childs.Add(_rightVerticalLine);

            _choke = new WellChoke(StyleCollection.chokeStyle);
            childs.Add(_choke);

            _chokeDiam = new ChokeDiamEllipse(this.source.Choke, StyleCollection.chokeEllipseStyle);
            childs.Add(_chokeDiam);

            //Обработка событий
            _choke.OnStateChangeRequest += (_, __) => JumperState = JumperState.nextState();
        }

        public void updateGeometry(List<KeyValuePair<PointF, List<PointF>>> points) {
            var pipeLinePoints = points.Select(pair => {
                var lineLocation = pair.Key;
                var delta = new PointF(lineLocation.X, lineLocation.Y);
                return pair.Value.Select(pt => new PointF(pt.X + delta.X, pt.Y + delta.Y)).ToList();
            }).ToList();

            var endPoint = new PointF(SeparatorJumperAnchorPoint.X + SectionsCount*StyleCollection.SEPARATORS_DISTANCE,
                SeparatorJumperAnchorPoint.Y);
            _line.update(SeparatorJumperAnchorPoint, endPoint, pipeLinePoints);

            var chokeCenterPoint =
                new PointF(SeparatorJumperAnchorPoint.X + (endPoint.X - SeparatorJumperAnchorPoint.X)/2,
                    SeparatorJumperAnchorPoint.Y);
            if (SectionsCount%2 == 0) {
                chokeCenterPoint.X += StyleCollection.SEPARATORS_DISTANCE/2;
            }
            _choke.clearTransformation();
            _choke.AnchorPoint = _choke.Center = chokeCenterPoint;
            _choke.rotate(90);

            _chokeDiam.Center = new PointF(chokeCenterPoint.X,
                chokeCenterPoint.Y + StyleCollection.CHOKE_ELLIPSE_CONNECTOR_LENGTH + _chokeDiam.Size.Height/2);

            var point1 = Separator1.SeparatorAnchorPoint;
            var point2 = Separator2.SeparatorAnchorPoint;
            _leftVerticalLine.Points.Clear();
            _leftVerticalLine.Points.AddRange(new[]
            {
                new PointF(Math.Min(point1.X, point2.X),
                    point1.Y - Separator1.SeparatorInfoHeight - Separator1.EllipseDiameter/2),
                SeparatorJumperAnchorPoint
            });

            _rightVerticalLine.Points.Clear();
            _rightVerticalLine.Points.AddRange(new[]
            {
                new PointF(Math.Max(point1.X, point2.X),
                    point1.Y - Separator1.SeparatorInfoHeight - Separator1.EllipseDiameter/2),
                endPoint
            });
        }
    }
}