﻿using System.Drawing;
using DistributionNetwork.dto;
using DistributionNetwork.gdi.primitives;
using DistributionNetwork.styles;
using PhotonGraphics.elements.baseElements;

namespace DistributionNetwork.gdi {
    /// <summary>
    /// Элемент скважины на мнемосхеме
    /// </summary>
    public class Well : GdiGroup {
        public readonly WellDto source;
        private readonly ChokeDiamEllipse _chokeDiamEllipse;
        private readonly ParamsBlob _paramsRect;
        private readonly NameRectangle _nameRectangle;
        private readonly WellChoke _choke;
        private ObjectState _wellState = ObjectState.osEnabled;
        private ObjectState _chokeState = ObjectState.osEnabled;
        private ObjectState _parentState = ObjectState.osEnabled;


        public int CollectorOrderNumber { get; private set; }

        public bool ChokeOnLeft { get; set; }

        public ObjectState WellState
        {
            get { return _wellState; }
            set
            {
                _wellState = value;
                if (ChokeState == ObjectState.osEnabled &&
                    ParentState == ObjectState.osEnabled) {
                    _nameRectangle.State = value;
                    _paramsRect.State = value;
                } else {
                    if (_wellState == ObjectState.osAlarm) {
                        _nameRectangle.State = ObjectState.osAlarm;
                    } else {
                        _nameRectangle.State = ObjectState.osDisabled;
                    }
                    _paramsRect.State = ObjectState.osDisabled;
                }
            }
        }

        public ObjectState ChokeState
        {
            get { return _chokeState; }
            set
            {
                _chokeState = value;
                if (ParentState == ObjectState.osEnabled) {
                    _choke.State = value;
                    _chokeDiamEllipse.State = value;
                } else {
                    if (_chokeState == ObjectState.osAlarm) {
                        _choke.State = ObjectState.osAlarm;
                    } else {
                        _choke.State = ObjectState.osDisabled;
                    }
                    _chokeDiamEllipse.State = ObjectState.osDisabled;
                }
                WellState = WellState;
            }
        }

        public ObjectState ParentState
        {
            get { return _parentState; }
            set
            {
                _parentState = value;
                ChokeState = ChokeState;
            }
        }

        public PointF WellAnchorPoint
        {
            get
            {
                var location = Location;
                var size = Size;
                return
                    new PointF(
                        ChokeOnLeft
                            ? location.X + size.Width - StyleCollection.WELL_NAME_RECT_WIDTH/2
                            : location.X + StyleCollection.WELL_NAME_RECT_WIDTH/2,
                        location.Y + size.Height);
            }
            set
            {
                var size = Size;
                Location =
                    new PointF(
                        ChokeOnLeft
                            ? value.X - size.Width + StyleCollection.WELL_NAME_RECT_WIDTH/2
                            : value.X - StyleCollection.WELL_NAME_RECT_WIDTH/2,
                        value.Y - size.Height);
            }
        }

        public Well(WellDto source, bool chokeOnLeft, int collectorOrderNumber) {
            this.source = source;
            ChokeOnLeft = chokeOnLeft;
            CollectorOrderNumber = collectorOrderNumber;

            //Прямоугольник с именем скважины
            _nameRectangle = new NameRectangle(this.source, StyleCollection.itemNameRectangleStyle);
            _nameRectangle.Center = new PointF(0, 0);
            _nameRectangle.AnchorPoint = new PointF(0, 0);
            _nameRectangle.HasConstantSize = true;

            //Линия до штуцера
            var line1 = GdiObjectFactory.createPolyLine(collectorOrderNumber);
            line1.Points.AddRange(new[]
            {
                new PointF(0, _nameRectangle.Style.Height/2),
                new PointF(0, _nameRectangle.Style.Height/2 + StyleCollection.CHOKE_TOP_CONNECTOR_LENGTH)
            });

            //Прямоугольник с параметрами
            _paramsRect = new ParamsBlob(this.source, StyleCollection.wellParamsStyle) {Direct = ChokeOnLeft ? ParamsBoubleDirect.RightUp : ParamsBoubleDirect.LeftUp};
            var paramsRectX = _nameRectangle.Size.Width/2 + StyleCollection.ELEMENTS_SPACING;
            if (ChokeOnLeft) {
                paramsRectX = -paramsRectX;
            }

            _paramsRect.BlobAnchorPoint = new PointF(paramsRectX, StyleCollection.BLOB_DISTANCE);
            _paramsRect.HasConstantSize = true;
            _paramsRect.AnchorPoint = _nameRectangle.Center;

            //Штуцер
            _choke = new WellChoke(StyleCollection.chokeStyle);
            _choke.Location = new PointF(-_choke.Style.Width/2,
                _nameRectangle.Style.Height/2 + StyleCollection.CHOKE_TOP_CONNECTOR_LENGTH);

            float startLineY = _nameRectangle.Style.Height/2 + StyleCollection.CHOKE_TOP_CONNECTOR_LENGTH +
                               _choke.Style.Height;

            //Круг с параметрами
            _chokeDiamEllipse = new ChokeDiamEllipse(this.source.ChokeInfo, StyleCollection.chokeEllipseStyle)
            {Tag = this.source};
            float infoX = ChokeOnLeft ?
                -StyleCollection.CHOKE_ELLIPSE_CONNECTOR_LENGTH - _chokeDiamEllipse.Style.Size :
                StyleCollection.CHOKE_ELLIPSE_CONNECTOR_LENGTH;
            float infoY = _nameRectangle.Style.Height/2 + StyleCollection.CHOKE_TOP_CONNECTOR_LENGTH +
                          (_choke.Style.Height - _chokeDiamEllipse.Style.Size)/2;
            _chokeDiamEllipse.Location = new PointF(infoX, infoY);

            //Линия после штуцера
            var line4 = GdiObjectFactory.createPolyLine(collectorOrderNumber);
            line4.Points.AddRange(new[]
            {
                new PointF(0, startLineY),
                new PointF(0, startLineY + StyleCollection.CHOKE_BOTTOM_CONNECTOR_LENGTH)
            });

            childs.Add(line1);
            childs.Add(line4);
            childs.Add(_chokeDiamEllipse);
            childs.Add(_nameRectangle);
            childs.Add(_paramsRect);
            childs.Add(_choke);

            //Обработка событий
            _nameRectangle.OnStateChangeRequest += (_, __) => WellState = WellState.nextState();
            _choke.OnStateChangeRequest += (_, __) => ChokeState = ChokeState.nextState();
        }
    }
}