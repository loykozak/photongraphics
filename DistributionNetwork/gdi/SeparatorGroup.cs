﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using DistributionNetwork.dto;
using DistributionNetwork.styles;

namespace DistributionNetwork.gdi {
    /// <summary>
    /// Группа одноименных сепараторов
    /// </summary>
    public class SeparatorGroup : List<Separator> {
        public bool ConnectionOnLeft { get; set; }

        /// <summary>
        /// Отступ по вертикали от имен сепараторов
        /// </summary>
        public float GroupVerticalPosition { get; set; }

        /// <summary>
        /// Общая высота элементов (не учитывает элементы имени сепаратора)
        /// </summary>
        public float GroupHeight
        {
            get
            {
                var elementsCount = this.Sum(i => i.RigsCount);
                elementsCount += this.Count(i => !i.WellListHeight.Equals(0));
                var result = this.Sum(i => {
                    var heihgt = i.WellListHeight;
                    heihgt += Enumerable.Range(0, i.RigsCount).Sum(idx => i.getRigHeight(idx));
                    return heihgt;
                });
                var differentRigsCount = this.SelectMany(i => i.RigList.Select(r => r.source.Name)).Distinct().Count();
                result += (elementsCount - 1)*StyleCollection.ELEMENTS_SPACING +
                          differentRigsCount*StyleCollection.ELEMENTS_SPACING*10; //todo move constant to styles
                var hasAnyFreeWells = this.Any(i => !i.WellListHeight.Equals(0));
                if (hasAnyFreeWells) {
                    result += StyleCollection.ELEMENTS_SPACING*20; //todo move constant to styles
                }
                return result;
            }
        }

        /// <summary>
        /// Вертикальные линии шлейфов в группе
        /// </summary>
        public List<KeyValuePair<PointF, List<PointF>>> GroupPipeLineInfos
        {
            get { return this.Select(i => new KeyValuePair<PointF, List<PointF>>(i.SeparatorAnchorPoint, i.PipeLine.Points)).ToList(); }
        }

        public SeparatorGroup(bool connectionOnLeft, IEnumerable<KeyValuePair<int, SeparatorDto>> separators) {
            ConnectionOnLeft = connectionOnLeft;
            var list = separators.Select(i => new Separator(i.Value, connectionOnLeft) {PositionIndex = i.Key}).ToList();
            this.AddRange(list);
        }

        /// <summary>
        /// Обновление геометрической расстановки объектов
        /// </summary>
        public void updateGeometry() {
            var startPos = GroupVerticalPosition;
            List<Separator> sepList = this;
            if (!ConnectionOnLeft) {
                sepList = this.OrderByDescending(i => i.PositionIndex).ToList();
            }
            var orderedRigObjects = sepList.SelectMany((i, sepIdx) => i.RigList
                .Select((rig, index) => new
                {
                    Separator = i,
                    SepIndex = sepIdx,
                    Rig = rig,
                    RigIndex = index
                }))
                .OrderBy(obj => obj.Rig.source.Name)
                .ToList();
            foreach (var rigObj in orderedRigObjects) {
                rigObj.Separator.SeparatorRelativePositions[rigObj.RigIndex] =
                    new SizeF(rigObj.SepIndex*StyleCollection.SEPARATORS_DISTANCE + 50, startPos);
                    //todo move constant to styles
                startPos += rigObj.Separator.getRigHeight(rigObj.RigIndex) + StyleCollection.ELEMENTS_SPACING;
                if (rigObj.SepIndex == this.Count - 1) {
                    startPos += StyleCollection.ELEMENTS_SPACING*10; //todo move constant to styles
                }
            }
            foreach (var separator in sepList) {
                if (separator.WellListHeight.Equals(0)) {
                    separator.PipeLineLength = separator.SeparatorRelativePositions.Max(i => i.Height);
                } else {
                    separator.PipeLineLength = startPos;
                    startPos += separator.WellListHeight + StyleCollection.ELEMENTS_SPACING;
                }
            }
            foreach (var separator in sepList) {
                separator.updatePipeLine();
            }
            foreach (var separator in sepList) {
                separator.updateGeometry(GroupPipeLineInfos);
            }
        }
    }
}