﻿using System;
using System.Drawing;
using DistributionNetwork.dto;
using DistributionNetwork.styles;
using DistributionNetwork.gdi.primitives;
using PhotonGraphics.elements.baseElements;

namespace DistributionNetwork.gdi {
    public class Rig : GdiGroup {
        public readonly RigDto source;
        private readonly InfoEllipse _infoEllipse;
        private readonly WellList _wellList;
        private readonly ParamsBlob _paramsRect;

        private ObjectState _rigState = ObjectState.osEnabled;
        private ObjectState _parentState = ObjectState.osEnabled;

        public int CollectorOrderNumber { get; private set; }

        public bool ConnectionOnLeft { get; set; }

        public PointF RigAnchorPoint
        {
            get
            {
                var location = Location;
                var size = Size;
                return new PointF(ConnectionOnLeft ? location.X + size.Width : location.X,
                    location.Y + size.Height - _paramsRect.Location.Y - _paramsRect.Size.Height); //todo debug
            }
            set
            {
                var size = Size;
                Location =
                    new PointF(
                        ConnectionOnLeft
                            ? value.X - size.Width
                            : value.X,
                        value.Y - size.Height + _paramsRect.Location.Y + _paramsRect.Size.Height);
            }
        }

        public ObjectState RigState
        {
            get { return _rigState; }
            set
            {
                _rigState = value;
                if (ParentState == ObjectState.osEnabled) {
                    _infoEllipse.State = value;
                    _paramsRect.State = value;
                    _wellList.ParentState = value;
                } else {
                    if (_rigState == ObjectState.osAlarm) {
                        _infoEllipse.State = ObjectState.osAlarm;
                    } else {
                        _infoEllipse.State = ObjectState.osDisabled;
                    }
                    _paramsRect.State = ObjectState.osDisabled;
                    _wellList.ParentState = ObjectState.osDisabled;
                }
            }
        }

        public ObjectState ParentState
        {
            get { return _parentState; }
            set
            {
                _parentState = value;
                RigState = RigState;
            }
        }

        public Rig(RigDto source, bool connectionOnLeft, int collectorOrderNumber) {
            this.source = source;
            ConnectionOnLeft = connectionOnLeft;
            CollectorOrderNumber = collectorOrderNumber;

            //Список скважин
            _wellList = new WellList(this.source.Wells, ConnectionOnLeft, collectorOrderNumber)
            {
                WellListAnchorPoint =
                    new PointF(ConnectionOnLeft ? -StyleCollection.RIG_ELLIPSE_SIZE : StyleCollection.RIG_ELLIPSE_SIZE,
                        0)
            };

            //Круг с именеи шлейфа
            _infoEllipse = new InfoEllipse(this.source, StyleCollection.rigEllipseStyle)
            {
                Tag = this.source,
                Center = new PointF(0, 0)
            };

            //Прямоугольник с параметрами
            _paramsRect = new ParamsBlob(this.source, StyleCollection.rigParamsStyle) {Direct = ParamsBoubleDirect.Up};
            _paramsRect.BlobAnchorPoint = new PointF(_infoEllipse.Center.X, 
                _infoEllipse.Center.Y + _infoEllipse.Size.Height/2 + StyleCollection.BLOB_DISTANCE);

            //Линия после круга
            var line = GdiObjectFactory.createPolyLine(collectorOrderNumber);
            line.Points.AddRange(new[]
            {
                new PointF(
                    ConnectionOnLeft
                        ? _paramsRect.Size.Width/2 + StyleCollection.ELEMENTS_SPACING
                        : -_paramsRect.Size.Width/2 - StyleCollection.ELEMENTS_SPACING,
                    0),
                _wellList.WellListAnchorPoint
            });

            childs.Add(line);
            childs.Add(_wellList);
            childs.Add(_infoEllipse);
            childs.Add(_paramsRect);

            //Обработка событий
            _infoEllipse.OnStateChangeRequest += (_, __) => RigState = RigState.nextState();
        }
    }
}