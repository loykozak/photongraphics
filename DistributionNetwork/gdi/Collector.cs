﻿using System;
using System.Collections.Generic;
using System.Drawing;
using DistributionNetwork.dto;
using DistributionNetwork.gdi.primitives;
using DistributionNetwork.styles;
using PhotonGraphics.elements.baseElements;

namespace DistributionNetwork.gdi {
    public class Collector : GdiGroup {
        public readonly CollectorDto source;

        private readonly InfoEllipse _infoEllipse;
        private readonly ParamsBlob _paramsRect;

        private ObjectState _collectorState = ObjectState.osEnabled;
        private ObjectState _parentState = ObjectState.osEnabled;

        public List<Separator> Separators { get; set; }

        public List<SeparatorJumper> SeparatorJumpers { get; set; }

        public PointF CollectorAnchorPoint
        {
            get
            {
                var maxWidth = Math.Max(_infoEllipse.Size.Width, _paramsRect.Size.Width);
                var location = Location;
                return new PointF(location.X + maxWidth/2, location.Y + _infoEllipse.Size.Height/2);
            }
            set
            {
                var maxWidth = Math.Max(_infoEllipse.Size.Width, _paramsRect.Size.Width);
                Location = new PointF(value.X - maxWidth/2, value.Y - _infoEllipse.Size.Height/2);
            }
        }

        public ObjectState CollectorState
        {
            get { return _collectorState; }
            set
            {
                _collectorState = value;
                if (ParentState == ObjectState.osEnabled) {
                    _infoEllipse.State = value;
                    _paramsRect.State = value;
                    foreach (var separator in Separators) {
                        separator.ParentState = value;
                    }
                    foreach (var separatorJumper in SeparatorJumpers) {
                        separatorJumper.ParentState = value;
                    }
                } else {
                    if (_collectorState == ObjectState.osAlarm) {
                        _infoEllipse.State = ObjectState.osAlarm;
                    } else {
                        _infoEllipse.State = ObjectState.osDisabled;
                    }
                    _paramsRect.State = ObjectState.osDisabled;
                    foreach (var separator in Separators) {
                        separator.ParentState = ObjectState.osDisabled;
                    }
                    foreach (var separatorJumper in SeparatorJumpers) {
                        separatorJumper.ParentState = ObjectState.osDisabled;
                    }
                }
            }
        }

        public ObjectState ParentState
        {
            get { return _parentState; }
            set
            {
                _parentState = value;
                CollectorState = CollectorState;
            }
        }

        public Collector(CollectorDto source) {
            this.source = source;

            //Эллипс с именем коллектора
            _infoEllipse = new InfoEllipse(this.source, StyleCollection.collectorEllipseStyle)
            {
                Tag = this.source,
                Center = new PointF(0, 0)
            };
            childs.Add(_infoEllipse);

            //Прямоугольник с параметрами коллектора
            _paramsRect = new ParamsBlob(this.source, StyleCollection.separatorParamsStyle)
            {
                Direct = ParamsBoubleDirect.Up,
                BlobAnchorPoint = new PointF(0,
                    _infoEllipse.Size.Height/2 + StyleCollection.BLOB_DISTANCE)
            };
            childs.Add(_paramsRect);

            //Обработка событий
            _infoEllipse.OnStateChangeRequest += (_, __) => CollectorState = CollectorState.nextState();
        }
    }
}