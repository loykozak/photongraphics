﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using DistributionNetwork.dto;
using DistributionNetwork.gdi.primitives;
using DistributionNetwork.styles;
using PhotonGraphics.elements.baseElements;
using PhotonGraphics.elements.shapes;

namespace DistributionNetwork.gdi {
    public class Separator : GdiGroup {
        public readonly SeparatorDto source;

        private readonly GdiPolyline _pipeLine;
        private readonly JumpLine[] _separatorLines;
        private readonly InfoEllipse _infoEllipse;
        private readonly ParamsBlob _paramsRect;
        private readonly WellList _wellList;
        private readonly Rig[] _rigList;

        private ObjectState _separatorState = ObjectState.osEnabled;
        private ObjectState _parentState = ObjectState.osEnabled;

        public float PipeLineLength { get; set; }

        public int PositionIndex { get; set; }

        public SizeF[] SeparatorRelativePositions { get; private set; }

        public bool ConnectionOnLeft { get; set; }

        public float WellListHeight
        {
            get { return _wellList == null ? 0 : _wellList.Size.Height; }
        }

        public int RigsCount
        {
            get { return _rigList.Length; }
        }

        public Rig[] RigList
        {
            get { return _rigList; }
        }

        public GdiPolyline PipeLine
        {
            get { return _pipeLine; }
        }

        public PointF SeparatorAnchorPoint
        {
            get
            {
                float el2 = _paramsRect.Size.Width/2;
                return new PointF(ConnectionOnLeft ? Location.X + Size.Width - el2 : Location.X + el2,
                    Location.Y + Size.Height);
            }
            set
            {
                float el2 = _paramsRect.Size.Width/2;
                Location = new PointF(ConnectionOnLeft ? value.X - Size.Width + el2 : value.X - el2,
                    value.Y - Size.Height);
            }
        }

        public float SeparatorInfoHeight
        {
            get { return _paramsRect.Location.Y + _paramsRect.Size.Height; }
        }

        public float EllipseDiameter
        {
            get { return _infoEllipse.Size.Height; }
        }

        public ObjectState SeparatorState
        {
            get { return _separatorState; }
            set
            {
                _separatorState = value;

                Action<ObjectState> setChildsState = st => {
                    if (_wellList != null) {
                        _wellList.ParentState = st;
                    }
                    foreach (var rig in _rigList) {
                        rig.ParentState = st;
                    }
                };

                if (ParentState == ObjectState.osEnabled) {
                    _infoEllipse.State = value;
                    _paramsRect.State = value;
                    setChildsState(value);
                } else {
                    if (_separatorState == ObjectState.osAlarm) {
                        _infoEllipse.State = ObjectState.osAlarm;
                    } else {
                        _infoEllipse.State = ObjectState.osDisabled;
                    }
                    _paramsRect.State = ObjectState.osDisabled;
                    setChildsState(ObjectState.osDisabled);
                }
            }
        }

        public ObjectState ParentState
        {
            get { return _parentState; }
            set
            {
                _parentState = value;
                SeparatorState = SeparatorState;
            }
        }

        public Separator(SeparatorDto source, bool connectionOnLeft) {
            this.source = source;
            ConnectionOnLeft = connectionOnLeft;

            //Список скважин
            _wellList = null;
            if (this.source.Wells != null &&
                this.source.Wells.Any()) {
                _wellList = new WellList(this.source.Wells, ConnectionOnLeft, source.Collector.OrderNumber);
            }

            //Список кустов
            _rigList =
                this.source.Rigs.Where(rig => rig.Wells.Any())
                    .Select(rig => new Rig(rig, ConnectionOnLeft, source.Collector.OrderNumber))
                    .ToArray();
            SeparatorRelativePositions = new SizeF[_rigList.Length];
            _separatorLines =
                _rigList.Select(i => new JumpLine(GdiObjectFactory.createLinePen(source.Collector.OrderNumber)))
                    .ToArray();

            //Линия шлейфа
            _pipeLine = GdiObjectFactory.createPolyLine(source.Collector.OrderNumber);

            //Круг с параметрами сепаратора
            _infoEllipse = new InfoEllipse(this.source, StyleCollection.separatorEllipseStyle)
            {
                Tag = this.source,
                Center = new PointF(0, 0)
            };

            //Расстановка кустов и скважин
            float posY = StyleCollection.SEPARATOR_VERTICAL_DISTANCE + _infoEllipse.Size.Height/2;

            for (int i = 0; i < _rigList.Length; i++) {
                SeparatorRelativePositions[i] = new SizeF(0, posY);
                if (i < _rigList.Length - 1) {
                    posY += _rigList[i].Size.Height + StyleCollection.ELEMENTS_SPACING;
                }
            }
            if (_wellList != null) {
                posY += _wellList.Size.Height + StyleCollection.ELEMENTS_SPACING;
            }
            PipeLineLength = posY;

            //Прямоугольник с параметрами
            _paramsRect = new ParamsBlob(this.source, StyleCollection.separatorParamsStyle) {Direct = ParamsBoubleDirect.Up};
            _paramsRect.BlobAnchorPoint = new PointF(0, _infoEllipse.Size.Height / 2 + StyleCollection.BLOB_DISTANCE);

            childs.Add(_pipeLine);
            childs.AddRange(_separatorLines);
            childs.Add(_infoEllipse);
            childs.Add(_paramsRect);

            if (_wellList != null) {
                childs.Add(_wellList);
            }
            if (_rigList.Length > 0) {
                childs.AddRange(_rigList);
            }

            updatePipeLine();
            updateGeometry(null);

            //Обработка событий
            _infoEllipse.OnStateChangeRequest += (_, __) => SeparatorState = SeparatorState.nextState();
        }

        public void updatePipeLine() {
            //линия шлейфа
            _pipeLine.Points.Clear();
            _pipeLine.Points.AddRange(new[]
            {
                new PointF(0, -PipeLineLength),
                new PointF(0, _infoEllipse.Size.Height)
            });
        }

        public void updateGeometry(List<KeyValuePair<PointF, List<PointF>>> points) {
            //линии кустов
            List<List<PointF>> pipeLinePoints = null;
            var thisLocation = SeparatorAnchorPoint;
            if (points != null) {
                pipeLinePoints = points.Select(pair => {
                    var lineLocation = pair.Key;
                    var delta = new PointF(lineLocation.X - thisLocation.X, lineLocation.Y - thisLocation.Y);
                    return pair.Value.Select(pt => new PointF(pt.X + delta.X, pt.Y + delta.Y)).ToList();
                }).ToList();
            }

            for (int i = 0; i < _separatorLines.Length; i++) {
                var position = SeparatorRelativePositions[i];
                var line = _separatorLines[i];
                var linePoint1 = new PointF(ConnectionOnLeft ? -position.Width : position.Width, -position.Height);
                var linePoint2 = new PointF(0, -position.Height);

                line.update(linePoint1, linePoint2, pipeLinePoints);
            }

            //позиция списка скважин
            if (_wellList != null) {
                _wellList.WellListAnchorPoint = new PointF(0, -PipeLineLength);
            }

            //позиции кустов
            for (int i = 0; i < _rigList.Length; i++) {
                var position = SeparatorRelativePositions[i];
                _rigList[i].RigAnchorPoint = new PointF(ConnectionOnLeft ? -position.Width : position.Width,
                    -position.Height);
            }
        }

        public float getRigHeight(int rigNumber) {
            if (rigNumber < 0 ||
                rigNumber >= RigsCount) {
                return 0;
            }
            return _rigList[rigNumber].Size.Height;
        }
    }
}