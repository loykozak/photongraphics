﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using DistributionNetwork.dto;
using DistributionNetwork.styles;
using PhotonGraphics.elements.shapes;

namespace DistributionNetwork.gdi.primitives {
    public class ParamsRectangle : GdiRoundedRectangle {
        private readonly InfoDto _info;
        private ObjectState _state = ObjectState.osEnabled;

        public RoundedRectangleStyle Style { get; private set; }

        public ObjectState State
        {
            get { return _state; }
            set
            {
                if (value == _state) {
                    return;
                }
                _state = value;
                updateColor();
            }
        }

        public ParamsRectangle(InfoDto info, RoundedRectangleStyle style) {
            _info = info;
            Style = style;
            init();
            update();
        }

        public ParamsRectangle(RoundedRectangleStyle style) {
            Style = style;
            init();
            update();
        }


        private void init() {
            Enabled = Style.IsActive;
        }

        private void updateColor() {
            switch (State) {
                case ObjectState.osEnabled:
                    ShapeBrush = new SolidBrush(Style.InactiveColor);
                    break;
                case ObjectState.osDisabled:
                case ObjectState.osAlarm:
                    ShapeBrush = new SolidBrush(StyleCollection.DisabledColor);
                    break;
            }
        }

        public void update() {
            Radius = Style.Radius;
            Size = new SizeF(Style.Width, Style.Height);
            VerticalAlignment = VerticalAlignment.Center;
            HorizontalAlignment = HorizontalAlignment.Left;
            TextPadding = new Padding(3, 0, 3, 0);

            if (_info.Params != null &&
                _info.Params.Any()) {
                var textBrush = new SolidBrush(Style.TextColor);
                var fontRegular = new Font(StyleCollection.FONT_NAME, Style.TextSize, FontStyle.Regular);
                var fontBold = new Font(StyleCollection.FONT_NAME, Style.TextSize, FontStyle.Bold);

                TextBlocks = _info.Params
                    .SelectMany(param => new List<TextBlockInfo>
                    {
                        new TextBlockInfo
                        {
                            Text = param.Name,
                            Font = fontRegular,
                            Brush = textBrush
                        },
                        new TextBlockInfo
                        {
                            Text = string.Format("{0:0.#}", param.Value),
                            Font = fontBold,
                            Brush = textBrush
                        },
                        new TextBlockInfo
                        {
                            Text = string.Format("{0}\n", param.Units),
                            Font = fontRegular,
                            Brush = textBrush
                        }
                    })
                    .ToList();
            } else {
                TextBlocks = null;
            }

            DrawBorder = Style.ShowBorder;
            BorderPen = new Pen(StyleCollection.collectorLineColors[0], StyleCollection.LINE_WIDTH);

            DrawFill = true;
            updateColor();

            adjustSize();
        }
    }
}