﻿using System.Drawing;
using System.Drawing.Drawing2D;
using DistributionNetwork.styles;
using PhotonGraphics.elements.shapes;

namespace DistributionNetwork.gdi.primitives {
    /// <summary>
    /// Фабрика стилизованных графических примитивов
    /// </summary>
    public static class GdiObjectFactory {

        public static Pen createLinePen(int collectorOrderNumber = 0, bool lineForParams = false) {
            return new Pen(StyleCollection.collectorLineColors[collectorOrderNumber],
                lineForParams ? StyleCollection.PARAMS_LINE_WIDTH : StyleCollection.LINE_WIDTH)
            {
                LineJoin = LineJoin.Round,
                StartCap = LineCap.Round,
                EndCap = LineCap.Round
            };
        }

        public static GdiPolyline createPolyLine(int collectorOrderNumber = 0, bool lineForParams = false) {
            return new GdiPolyline
            {
                Pen = createLinePen(collectorOrderNumber, lineForParams)
            };
        }

        public static GdiArrow createLineArrow() {
            return new GdiArrow
            {
                Height = StyleCollection.ARROW_CAP_HEIGHT,
                Width = StyleCollection.ARROW_CAP_WIDTH,
                MiddleInset = StyleCollection.ARROW_CAP_INSET,
                Color = StyleCollection.collectorLineColors[0]
            };
        }
    }
}