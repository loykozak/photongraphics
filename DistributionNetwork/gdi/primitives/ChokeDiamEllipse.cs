﻿using System.Collections.Generic;
using System.Drawing;
using DistributionNetwork.dto;
using DistributionNetwork.styles;
using PhotonGraphics.elements.shapes;

namespace DistributionNetwork.gdi.primitives {
    /// <summary>
    /// Кружок с диаметром штуцера
    /// </summary>
    public class ChokeDiamEllipse : GdiEllipse {
        private readonly InfoDto _info;
        private ObjectState _state = ObjectState.osEnabled;

        public EllipseStyle Style { get; set; }

        public ObjectState State
        {
            get { return _state; }
            set
            {
                if (value == _state) {
                    return;
                }
                _state = value;
                updateColor();
            }
        }

        public ChokeDiamEllipse(InfoDto info, EllipseStyle style) {
            _info = info;
            Style = style;
            init();
            update();
        }

        private void init() {
            Enabled = Style.IsActive;
        }

        private void updateColor() {
            switch (State) {
                case ObjectState.osEnabled:
                    ShapeBrush = new SolidBrush(Style.InactiveColor);
                    break;
                case ObjectState.osDisabled:
                case ObjectState.osAlarm:
                    ShapeBrush = new SolidBrush(StyleCollection.DisabledColor);
                    break;
            }
        }

        private void update() {
            Size = new SizeF(Style.Size, Style.Size);

            if (_info.Params != null &&
                _info.Params.Count > 0) {
                var textBrush = new SolidBrush(Style.TextColor);
                var fontRegular = new Font(StyleCollection.FONT_NAME, Style.TextSize, FontStyle.Regular);
                var fontBold = new Font(StyleCollection.FONT_NAME, Style.TextSize, FontStyle.Bold);

                TextBlocks = new List<TextBlockInfo>
                {
                    new TextBlockInfo
                    {
                        Text = _info.Params[0].Name,
                        Font = fontRegular,
                        Brush = textBrush
                    },
                    new TextBlockInfo
                    {
                        Text = string.Format("{0:0.#}\n", _info.Params[0].Value),
                        Font = fontBold,
                        Brush = textBrush
                    },
                    new TextBlockInfo
                    {
                        Text = string.Format("{0}", _info.Params[0].Units),
                        Font = fontRegular,
                        Brush = textBrush
                    }
                };
            } else {
                TextBlocks = null;
            }

            DrawBorder = Style.ShowBorder;
            BorderPen = new Pen(StyleCollection.collectorLineColors[0], StyleCollection.LINE_WIDTH);

            DrawFill = true;
            updateColor();
        }
    }
}