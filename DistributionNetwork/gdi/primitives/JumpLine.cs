﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using DistributionNetwork.styles;
using PhotonGraphics.elements.baseElements;
using PhotonGraphics.elements.shapes;

namespace DistributionNetwork.gdi.primitives {
    public class JumpLine : GdiGroup {
        public Pen Pen { get; set; }

        public JumpLine(Pen linePen) {
            Pen = linePen;
        }

        public JumpLine(Pen linePen, PointF startPoint, PointF endPoint, IEnumerable<GdiPolyline> crossableLines)
            : this(linePen) {
            update(startPoint, endPoint, crossableLines);
        }

        public void update(PointF startPoint, PointF endPoint, IEnumerable<GdiPolyline> crossableLines) {
            update(startPoint, endPoint, crossableLines == null ? null : crossableLines.Select(i => i.Points));
        }

        public void update(PointF startPoint, PointF endPoint, IEnumerable<List<PointF>> linePoints) {
            childs.Clear();
            var line = new GdiPolyline {Pen = Pen};
            line.Points.AddRange(new[]
            {
                startPoint,
                endPoint
            });
            childs.Add(line);
            if (linePoints != null) {
                foreach (var lineList in linePoints) {
                    if (lineList.Count > 1) {
                        for (int i = 1; i < lineList.Count; i++) {
                            if (isPointOnSegment(lineList[i - 1], startPoint, endPoint) ||
                                isPointOnSegment(lineList[i], startPoint, endPoint) ||
                                isPointOnSegment(startPoint, lineList[i - 1], lineList[i]) ||
                                isPointOnSegment(endPoint, lineList[i - 1], lineList[i])) {
                                continue;
                            }
                            PointF crossPoint;
                            if (calcIntersectionPoint(startPoint, endPoint, lineList[i - 1],
                                lineList[i],
                                out crossPoint)) {
                                var jumpElement = new JumpElement
                                {
                                    Pen = Pen,
                                    Brush = new SolidBrush(StyleCollection.backgroundColor),
                                    Radius = StyleCollection.ROUNED_CROSS_RADIUS,
                                    Location = crossPoint
                                };
                                childs.Add(jumpElement);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Находит точку пересечения двух отрезков на плоскости
        /// </summary>
        /// <param name="line1Start"></param>
        /// <param name="line1End"></param>
        /// <param name="line2Start"></param>
        /// <param name="line2End"></param>
        /// <param name="intersectionPoint"></param>
        /// <returns></returns>
        private static bool calcIntersectionPoint(PointF line1Start,
            PointF line1End,
            PointF line2Start,
            PointF line2End,
            out PointF intersectionPoint) {
            var vector1 = new PointF(line1End.X - line1Start.X, line1End.Y - line1Start.Y);
            var vector2 = new PointF(line2End.X - line2Start.X, line2End.Y - line2Start.Y);

            //считаем уравнения прямых проходящих через отрезки
            var a1 = -vector1.Y;
            var b1 = vector1.X;
            var d1 = -(a1*line1Start.X + b1*line1Start.Y);

            var a2 = -vector2.Y;
            var b2 = vector2.X;
            var d2 = -(a2*line2Start.X + b2*line2Start.Y);

            //подставляем концы отрезков, для выяснения в каких полуплоскотях они
            var seg1Line2Start = a2*line1Start.X + b2*line1Start.Y + d2;
            var seg1Line2End = a2*line1End.X + b2*line1End.Y + d2;

            var seg2Line1Start = a1*line2Start.X + b1*line2Start.Y + d1;
            var seg2Line1End = a1*line2End.X + b1*line2End.Y + d1;

            //если концы одного отрезка имеют один знак, значит он в одной полуплоскости и пересечения нет.
            if (seg1Line2Start*seg1Line2End >= 0 ||
                seg2Line1Start*seg2Line1End >= 0) {
                intersectionPoint = new PointF();
                return false;
            }

            var u = seg1Line2Start/(seg1Line2Start - seg1Line2End);
            intersectionPoint = new PointF(line1Start.X + u*vector1.X, line1Start.Y + u*vector1.Y);

            return true;
        }

        /// <summary>
        /// Проверка лежит ли точка на отрезке
        /// </summary>
        /// <param name="point"></param>
        /// <param name="lineStart"></param>
        /// <param name="lineEnd"></param>
        /// <returns></returns>
        private static bool isPointOnSegment(PointF point, PointF lineStart, PointF lineEnd) {
            if (lineStart.X.equalsWithDelta(lineEnd.X)) {
                return (point.X.equalsWithDelta(lineStart.X) && point.Y >= Math.Min(lineStart.Y, lineEnd.Y) &&
                        point.Y <= Math.Max(lineStart.Y, lineEnd.Y));
            }
            var k = (lineEnd.Y - lineStart.Y)/(lineEnd.X - lineStart.X);
            var c = lineStart.Y - k*lineStart.X;
            return point.Y.equalsWithDelta(point.X*k + c);
        }
        
    }

    //todo решить проблему с погрешностями
    internal static class JumpLineExtension {
        public static bool equalsWithDelta(this float val1, float val2)
        {
            return Math.Abs(val1 - val2) < 0.001;
        }
    }
}