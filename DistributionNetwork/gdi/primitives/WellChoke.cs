﻿using System;
using System.Drawing;
using DistributionNetwork.dto;
using DistributionNetwork.styles;
using PhotonGraphics.elements.baseElements;
using PhotonGraphics.elements.shapes;

namespace DistributionNetwork.gdi.primitives {
    /// <summary>
    /// Штуцер
    /// </summary>
    public class WellChoke : GdiPolygon {
        private ObjectState _state = ObjectState.osEnabled;

        public event EventHandler OnStateChangeRequest;

        public RectangleStyle Style { get; set; }

        public ObjectState State
        {
            get { return _state; }
            set
            {
                if (value == _state) {
                    return;
                }
                _state = value;
                updateColor();
            }
        }

        public WellChoke(RectangleStyle style) {
            Style = style;
            init();
            update();
        }

        private void init() {
            Enabled = Style.IsActive;
            MouseHover += (_, __) => updateColor();
            MouseLeave += (_, __) => updateColor();
            MouseDown += (_, __) => updateColor();
            MouseUp += (_, __) => updateColor();
            MouseDoubleClick += (_, __) => {
                var h = OnStateChangeRequest;
                if (h != null) {
                    h(this, EventArgs.Empty);
                }
            };
        }

        private void setColor(Color color) {
            ShapeBrush = new SolidBrush(color);
            BorderPen.Color = color;
        }

        private void updateColor() {
            switch (State) {
                case ObjectState.osEnabled:
                    switch (MouseState)
                    {
                        case ActiveElementState.aesActive:
                            setColor(Style.ActiveColor);
                            break;
                        case ActiveElementState.aesInactive:
                            setColor(Style.InactiveColor);
                            break;
                        case ActiveElementState.aesHovered:
                            setColor(Style.HoveredColor);
                            break;
                    }
                    break;
                case ObjectState.osDisabled:
                    setColor(StyleCollection.DisabledColor);
                    break;
                case ObjectState.osAlarm:
                    setColor(StyleCollection.AlarmColor);
                    break;
            }
        }

        private void update() {
            Size = new SizeF(Style.Width, Style.Height);

            DrawBorder = Style.ShowBorder;
            DrawFill = true;

            updateColor();

            Points.Clear();
            Points.AddRange(new[]
            {
                new PointF(0, 0),
                new PointF(Style.Width, 0),
                new PointF(0, Style.Height),
                new PointF(Style.Width, Style.Height)
            });
        }
    }
}