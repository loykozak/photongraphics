﻿using System;
using System.Collections.Generic;
using System.Drawing;
using DistributionNetwork.dto;
using DistributionNetwork.styles;
using PhotonGraphics.elements.baseElements;
using PhotonGraphics.elements.shapes;

namespace DistributionNetwork.gdi.primitives {
    /// <summary>
    /// Круг с надписью
    /// </summary>
    public class InfoEllipse : GdiEllipse {
        private readonly InfoDto _info;
        private ObjectState _state = ObjectState.osEnabled;

        public event EventHandler OnStateChangeRequest;

        public EllipseStyle Style { get; set; }

        public ObjectState State
        {
            get { return _state; }
            set
            {
                if (value == _state) {
                    return;
                }
                _state = value;
                updateColor();
            }
        }

        public InfoEllipse(InfoDto info, EllipseStyle style) {
            _info = info;
            Style = style;
            init();
            update();
        }

        private void init() {
            Enabled = Style.IsActive;
            MouseHover += (_, __) => updateColor();
            MouseLeave += (_, __) => updateColor();
            MouseDown += (_, __) => updateColor();
            MouseUp += (_, __) => updateColor();
            MouseDoubleClick += (_, __) => {
                var h = OnStateChangeRequest;
                if (h != null) {
                    h(this, EventArgs.Empty);
                }
            };
        }

        private void updateColor() {
            switch (State) {
                case ObjectState.osEnabled:
                    switch (MouseState) {
                        case ActiveElementState.aesActive:
                            ShapeBrush = new SolidBrush(Style.ActiveColor);
                            break;
                        case ActiveElementState.aesInactive:
                            ShapeBrush = new SolidBrush(Style.InactiveColor);
                            break;
                        case ActiveElementState.aesHovered:
                            ShapeBrush = new SolidBrush(Style.HoveredColor);
                            break;
                    }
                    break;
                case ObjectState.osDisabled:
                    ShapeBrush = new SolidBrush(StyleCollection.DisabledColor);
                    break;
                case ObjectState.osAlarm:
                    ShapeBrush = new SolidBrush(StyleCollection.AlarmColor);
                    break;
            }
        }

        private void update() {
            Size = new SizeF(Style.Size, Style.Size);

            if (string.IsNullOrEmpty(_info.Title)) {
                Text = _info.Name;
                TextBrush = new SolidBrush(Style.TextColor);
                Font = new Font(StyleCollection.FONT_NAME, Style.TextSize, FontStyle.Bold);
            } else {
                var textBrush = new SolidBrush(Style.TextColor);
                var fontRegular = new Font(StyleCollection.FONT_NAME, Style.TextSize, FontStyle.Regular);
                var fontBold = new Font(StyleCollection.FONT_NAME, Style.TextSize, FontStyle.Bold);

                TextBlocks = new List<TextBlockInfo>
                {
                    new TextBlockInfo
                    {
                        Text = _info.Title + "\n",
                        Font = fontRegular,
                        Brush = textBrush
                    },
                    new TextBlockInfo
                    {
                        Text = _info.Name,
                        Font = fontBold,
                        Brush = textBrush
                    }
                };
            }

            DrawBorder = Style.ShowBorder;
            BorderPen = new Pen(StyleCollection.collectorLineColors[0], StyleCollection.LINE_WIDTH);

            DrawFill = true;
            updateColor();
        }
    }
}