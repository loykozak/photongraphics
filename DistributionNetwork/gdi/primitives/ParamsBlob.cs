﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using DistributionNetwork.dto;
using DistributionNetwork.styles;
using PhotonGraphics.elements.baseElements;

namespace DistributionNetwork.gdi.primitives {
    public class ParamsBlob : ParamsRectangle {
        public float ArrowHeight { get; private set; }
        public float ArrowWidth { get; private set; }
        public float ArrowOffset { get; private set; }
        private ParamsBoubleDirect _direct;

        public ParamsBoubleDirect Direct
        {
            get { return _direct; }
            set
            {
                _direct = value;
                update();
            }
        }

        public new ParamsBlobStyle Style { get; private set; }

        public ParamsBlob(InfoDto info, ParamsBlobStyle style) : base(info, style) {
            Style = style;
            _direct = ParamsBoubleDirect.None;
            update();
        }

        public PointF BlobAnchorPoint
        {
            get
            {
                var loc = Location;
                var size = Size;
                switch (Direct) {
                    case ParamsBoubleDirect.Up:
                        return new PointF(loc.X + size.Width/2, loc.Y - ArrowHeight);
                    case ParamsBoubleDirect.Down:
                        return new PointF(loc.X + size.Width/2, loc.Y + size.Height + ArrowHeight);
                    case ParamsBoubleDirect.Left:
                        return new PointF(loc.X - ArrowHeight, loc.Y + size.Height/2);
                    case ParamsBoubleDirect.Right:
                        return new PointF(loc.X + size.Width + ArrowHeight, loc.Y + size.Height/2);
                    case ParamsBoubleDirect.LeftUp:
                        return new PointF(loc.X + ArrowOffset, loc.Y - ArrowHeight);
                    case ParamsBoubleDirect.RightUp:
                        return new PointF(loc.X + size.Width - ArrowOffset, loc.Y - ArrowHeight);
                    case ParamsBoubleDirect.LeftDown:
                        return new PointF(loc.X + ArrowOffset, loc.Y + size.Height + ArrowHeight);
                    case ParamsBoubleDirect.RightDown:
                        return new PointF(loc.X + size.Width - ArrowOffset, loc.Y + size.Height + ArrowHeight);
                    default:
                        return Center;
                }
            }
            set
            {
                var size = Size;
                switch (Direct) {
                    case ParamsBoubleDirect.Up:
                        Location = new PointF(value.X - size.Width/2, value.Y + ArrowHeight);
                        break;
                    case ParamsBoubleDirect.Down:
                        Location = new PointF(value.X - size.Width/2, value.Y - size.Height - ArrowHeight);
                        break;
                    case ParamsBoubleDirect.Left:
                        Location = new PointF(value.X + ArrowHeight, value.Y - size.Height/2);
                        break;
                    case ParamsBoubleDirect.Right:
                        Location = new PointF(value.X - size.Width - ArrowHeight, value.Y - size.Height/2);
                        break;
                    case ParamsBoubleDirect.LeftUp:
                        Location = new PointF(value.X - ArrowOffset, value.Y + ArrowHeight);
                        break;
                    case ParamsBoubleDirect.RightUp:
                        Location = new PointF(value.X - size.Width + ArrowOffset, value.Y + ArrowHeight);
                        break;
                    case ParamsBoubleDirect.LeftDown:
                        Location = new PointF(value.X - ArrowOffset, value.Y - size.Height - ArrowHeight);
                        break;
                    case ParamsBoubleDirect.RightDown:
                        Location = new PointF(value.X - size.Width + ArrowOffset, value.Y - size.Height - ArrowHeight);
                        break;
                    default:
                        Center = value;
                        break;
                }
            }
        }

        private void update() {
            ArrowHeight = Style.ArrowHeight;
            ArrowWidth = Style.ArrowWidth;
            ArrowOffset = Style.ArrowCornerOffset;
//            if (Direct == ParamsBoubleDirect.None)
//                return;
//            switch (_direct) {
//                case ParamsBoubleDirect.Up:
//                case ParamsBoubleDirect.RightUp:
//                case ParamsBoubleDirect.LeftUp:
//                    TextPadding = new Padding(TextPadding.Left, TextPadding.Top + (int) Style.ArrowHeight,
//                        TextPadding.Right, TextPadding.Bottom);
//                    break;
//                case ParamsBoubleDirect.Down:
//                case ParamsBoubleDirect.LeftDown:
//                case ParamsBoubleDirect.RightDown:
//                    TextPadding = new Padding(TextPadding.Left, TextPadding.Top, TextPadding.Right,
//                        TextPadding.Bottom + (int) Style.ArrowHeight);
//                    break;
//                case ParamsBoubleDirect.Left:
//                    TextPadding = new Padding(TextPadding.Left + (int) Style.ArrowHeight, TextPadding.Top,
//                        TextPadding.Right, TextPadding.Bottom);
//                    break;
//                case ParamsBoubleDirect.Right:
//                    TextPadding = new Padding(TextPadding.Left, TextPadding.Top,
//                        TextPadding.Right + (int) Style.ArrowHeight, TextPadding.Bottom);
//                    break;
//            }
        }

        public override void render(LibGraphicsObject go) {
            var triangle = makeBoubleTriangle(RenderingRect);
            if (triangle != null) {
                if (DrawFill && ShapeBrush != null) {
                    go.Graphics.FillPath(ShapeBrush, triangle);
                }
                if (DrawBorder && BorderPen != null) {
                    var brush = new SolidBrush(BorderPen.Color);
                    go.Graphics.FillPath(brush, triangle);
                }
            }
            base.render(go);
        }

        private GraphicsPath makeBoubleTriangle(RectangleF bounds) {
            var path = new GraphicsPath();

            if (_direct == ParamsBoubleDirect.Up) {
                path.AddLine(new PointF(bounds.Left + bounds.Width/2 - ArrowWidth/2, bounds.Y),
                    new PointF(bounds.Left + bounds.Width/2, bounds.Y - ArrowHeight));
                path.AddLine(new PointF(bounds.Left + bounds.Width/2, bounds.Y - ArrowHeight),
                    new PointF(bounds.Left + bounds.Width/2 + ArrowWidth/2, bounds.Y));
            } else if (_direct == ParamsBoubleDirect.RightUp) {
                path.AddLine(new PointF(bounds.Right - ArrowOffset - ArrowWidth*1.5f, bounds.Y),
                    new PointF(bounds.Right - ArrowOffset, bounds.Y - ArrowHeight));
                path.AddLine(new PointF(bounds.Right - ArrowOffset, bounds.Y - ArrowHeight),
                    new PointF(bounds.Right - ArrowOffset - ArrowWidth/2, bounds.Y));
            } else if (_direct == ParamsBoubleDirect.LeftUp) {
                path.AddLine(new PointF(bounds.Left + ArrowOffset + ArrowWidth*1.5f, bounds.Y),
                    new PointF(bounds.Left + ArrowOffset, bounds.Y - ArrowHeight));
                path.AddLine(new PointF(bounds.Left + ArrowOffset, bounds.Y - ArrowHeight),
                    new PointF(bounds.Left + ArrowOffset + ArrowWidth/2, bounds.Y));
            } else if (_direct == ParamsBoubleDirect.Right) {
                path.AddLine(new PointF(bounds.Right, bounds.Y + bounds.Height/2 - ArrowWidth/2),
                    new PointF(bounds.Right + ArrowHeight, bounds.Y + bounds.Height/2));
                path.AddLine(new PointF(bounds.Right + ArrowHeight, bounds.Y + bounds.Height/2),
                    new PointF(bounds.Right, bounds.Y + bounds.Height/2 + ArrowWidth/2));
            } else if (_direct == ParamsBoubleDirect.Down) {
                path.AddLine(new PointF(bounds.Left + bounds.Width/2 + ArrowWidth/2, bounds.Bottom),
                    new PointF(bounds.Left + bounds.Width/2, bounds.Bottom + ArrowHeight));
                path.AddLine(new PointF(bounds.Left + bounds.Width/2, bounds.Bottom + ArrowHeight),
                    new PointF(bounds.Left + bounds.Width/2 - ArrowWidth/2, bounds.Bottom));
            } else if (_direct == ParamsBoubleDirect.RightDown) {
                path.AddLine(new PointF(bounds.Right - ArrowOffset - ArrowWidth/2, bounds.Bottom),
                    new PointF(bounds.Right - ArrowOffset, bounds.Bottom + ArrowHeight));
                path.AddLine(new PointF(bounds.Right - ArrowOffset, bounds.Bottom + ArrowHeight),
                    new PointF(bounds.Right - ArrowOffset - ArrowWidth*1.5f, bounds.Bottom));
            } else if (_direct == ParamsBoubleDirect.LeftDown) {
                path.AddLine(new PointF(bounds.Left + ArrowOffset + ArrowWidth/2, bounds.Bottom),
                    new PointF(bounds.Left + ArrowOffset, bounds.Bottom + ArrowHeight));
                path.AddLine(new PointF(bounds.Left + ArrowOffset, bounds.Bottom + ArrowHeight),
                    new PointF(bounds.Left + ArrowOffset + ArrowWidth*1.5f, bounds.Bottom));
            } else if (_direct == ParamsBoubleDirect.Left) {
                path.AddLine(new PointF(bounds.Left, bounds.Y + bounds.Height/2 + ArrowWidth/2),
                    new PointF(bounds.Left - ArrowHeight, bounds.Y + bounds.Height/2));
                path.AddLine(new PointF(bounds.Left - ArrowHeight, bounds.Y + bounds.Height/2),
                    new PointF(bounds.Left, bounds.Y + bounds.Height/2 - ArrowWidth/2));
            } else {
                return null;
            }

            return path;
        }
    }


    public enum ParamsBoubleDirect {
        None = 0,
        Up = 1,
        Down,
        Left,
        Right,
        LeftUp,
        RightUp,
        LeftDown,
        RightDown
    }
}