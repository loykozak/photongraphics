﻿using System.Drawing;
using System.Drawing.Drawing2D;
using PhotonGraphics.elements.baseElements;

namespace DistributionNetwork.gdi.primitives
{
    public class JumpElement : GdiElement
    {
        public Pen Pen { get; set; }

        public SolidBrush Brush { get; set; }

        public float Radius { get; set; }

        public override void render(LibGraphicsObject go) {
            if (Pen != null && Brush != null && Radius > 0f) {
                var pen = new Pen(Pen.Color, Pen.Width);
                var borderPent = new Pen(Brush.Color);
                var arc = new RectangleF(-Radius, -Radius, 2 * Radius, 2 * Radius);
                var path = new GraphicsPath();
                path.AddLine(new PointF(-Radius, Pen.Width/2), new PointF(-Radius, 0));
                path.AddArc(arc, 180, 180);
                path.AddLine(new PointF(Radius, 0), new PointF(Radius, Pen.Width / 2));

                var cloned = (GraphicsPath)path.Clone();
                cloned.CloseFigure();
                go.Graphics.FillPath(Brush, cloned);
                go.Graphics.DrawPath(borderPent, cloned);
                
                go.Graphics.DrawPath(getBorderPen(pen, go), path);
                
            }
        }
    }
}
