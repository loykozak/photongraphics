﻿using PhotonGraphics;

namespace DistributionNetwork {
    partial class FormTestGdi {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnFitSize = new System.Windows.Forms.Button();
            this.chkMouseZoom = new System.Windows.Forms.CheckBox();
            this.chkMouseScroll = new System.Windows.Forms.CheckBox();
            this.canvas1 = new PhotonGraphics.Canvas();
            this.topPanel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.Location = new System.Drawing.Point(0, 0);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(261, 515);
            this.propertyGrid1.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnFitSize);
            this.splitContainer1.Panel1.Controls.Add(this.chkMouseZoom);
            this.splitContainer1.Panel1.Controls.Add(this.chkMouseScroll);
            this.splitContainer1.Panel1.Controls.Add(this.canvas1);
            this.splitContainer1.Panel1.Controls.Add(this.topPanel);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.propertyGrid1);
            this.splitContainer1.Size = new System.Drawing.Size(965, 515);
            this.splitContainer1.SplitterDistance = 700;
            this.splitContainer1.TabIndex = 2;
            // 
            // btnFitSize
            // 
            this.btnFitSize.Location = new System.Drawing.Point(183, 4);
            this.btnFitSize.Name = "btnFitSize";
            this.btnFitSize.Size = new System.Drawing.Size(75, 23);
            this.btnFitSize.TabIndex = 5;
            this.btnFitSize.Text = "Fit size";
            this.btnFitSize.UseVisualStyleBackColor = true;
            this.btnFitSize.Click += new System.EventHandler(this.btnFitSizeClick);
            // 
            // chkMouseZoom
            // 
            this.chkMouseZoom.AutoSize = true;
            this.chkMouseZoom.Checked = true;
            this.chkMouseZoom.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMouseZoom.Location = new System.Drawing.Point(91, 8);
            this.chkMouseZoom.Name = "chkMouseZoom";
            this.chkMouseZoom.Size = new System.Drawing.Size(86, 17);
            this.chkMouseZoom.TabIndex = 3;
            this.chkMouseZoom.Text = "Mouse zoom";
            this.chkMouseZoom.UseVisualStyleBackColor = true;
            this.chkMouseZoom.CheckedChanged += new System.EventHandler(this.chkMouseZoomCheckedChanged);
            // 
            // chkMouseScroll
            // 
            this.chkMouseScroll.AutoSize = true;
            this.chkMouseScroll.Checked = true;
            this.chkMouseScroll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMouseScroll.Location = new System.Drawing.Point(4, 8);
            this.chkMouseScroll.Name = "chkMouseScroll";
            this.chkMouseScroll.Size = new System.Drawing.Size(85, 17);
            this.chkMouseScroll.TabIndex = 2;
            this.chkMouseScroll.Text = "Mouse scroll";
            this.chkMouseScroll.UseVisualStyleBackColor = true;
            this.chkMouseScroll.CheckedChanged += new System.EventHandler(this.chkMouseScrollCheckedChanged);
            // 
            // canvas1
            // 
            this.canvas1.BackColor = System.Drawing.Color.White;
            this.canvas1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.canvas1.Location = new System.Drawing.Point(0, 30);
            this.canvas1.Name = "canvas1";
            this.canvas1.RenderElement = null;
            this.canvas1.Size = new System.Drawing.Size(700, 485);
            this.canvas1.TabIndex = 0;
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.SystemColors.Control;
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(700, 30);
            this.topPanel.TabIndex = 1;
            // 
            // FormTestGdi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(965, 515);
            this.Controls.Add(this.splitContainer1);
            this.Name = "FormTestGdi";
            this.Text = "Distribution network";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.formTestGdiLoad);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Canvas canvas1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnFitSize;
        private System.Windows.Forms.CheckBox chkMouseZoom;
        private System.Windows.Forms.CheckBox chkMouseScroll;
        private System.Windows.Forms.Label topPanel;
        public System.Windows.Forms.PropertyGrid propertyGrid1;


    }
}