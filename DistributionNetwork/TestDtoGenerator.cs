﻿using System;
using System.Collections.Generic;
using System.Globalization;
using DistributionNetwork.dto;
using DistributionNetwork.gdi;

namespace DistributionNetwork {
    public static class TestDtoGenerator {
        private static readonly Random Random = new Random(DateTime.Now.Millisecond);

        public static WellDto createTestWell() {
            return new WellDto
            {
                Name = (2001 + Random.Next(100)).ToString(CultureInfo.InvariantCulture),
                Params = new List<InfoParamDto>
                {
                    new InfoParamDto
                    {
                        Name = "Tуст",
                        Value = Random.NextDouble()*200,
                        Units = "°C"
                    },
                    new InfoParamDto
                    {
                        Name = "Pуст",
                        Value = 40 + Random.NextDouble()*100,
                        Units = "кгс/см²"
                    },
                    new InfoParamDto
                    {
                        Name = "Qсм",
                        Value = 500 + Random.NextDouble()*500,
                        Units = "м³/сут"
                    }
                },
                ChokeInfo = new ChokeDto
                {
                    Params = new List<InfoParamDto>
                    {
                        new InfoParamDto
                        {
                            Name = "ø", //alt + 0248
                            Value = 15,
                            Units = "мм"
                        }
                    }
                }
            };
        }

        public static List<WellDto> createTestWellList() {
            int wellsCount = 1 + Random.Next(10);
            var list = new List<WellDto>();
            for (int i = 0; i < wellsCount; i++) {
                list.Add(createTestWell());
            }
            return list;
        }

        public static RigDto createTestRig(int regNumber) {
            return new RigDto
            {
                Title = "Куст",
                Name = regNumber.ToString(),
                Wells = createTestWellList(),
                Params = new List<InfoParamDto>
                {
                    new InfoParamDto
                    {
                        Name = "T",
                        Value = Random.NextDouble()*200,
                        Units = "°C"
                    },
                    new InfoParamDto
                    {
                        Name = "P",
                        Value = 40 + Random.NextDouble()*100,
                        Units = "кгс/см²"
                    }
                }
            };
        }

        public static SeparatorDto createTestSeparator(int sepNum, CollectorDto collector) {
            var sep = new SeparatorDto
            {
//                Name = string.Format("{0}\n{1}", sepNum, collector.Name),
                Name = sepNum.ToString(),
                Collector = collector,
                Params = new List<InfoParamDto>
                {
                    new InfoParamDto
                    {
                        Name = "T",
                        Value = Random.NextDouble()*200,
                        Units = "°C"
                    },
                    new InfoParamDto
                    {
                        Name = "P",
                        Value = 40 + Random.NextDouble()*100,
                        Units = "кгс/см²"
                    }
                }
            };
            if (Random.NextDouble() > 0.9) {
                sep.Wells = createTestWellList();
            }
            sep.Rigs.Add(createTestRig(201));
            if (Random.NextDouble() > 0.7) {
                sep.Rigs.Add(createTestRig(202));
            }
            if (Random.NextDouble() > 0.7) {
                sep.Rigs.Add(createTestRig(203));
            }
//            sep.Wells = createTestWellList();
//            sep.Rigs.Add(createTestRig(201));
//            sep.Rigs.Add(createTestRig(202));
//            sep.Rigs.Add(createTestRig(203));
            return sep;
        }

        public static CollectorDto createTestCollector(string name, int orderNumber) {
            var collector = new CollectorDto
            {
                OrderNumber = orderNumber,
                Name = name,
                Params = new List<InfoParamDto>
                {
                    new InfoParamDto
                    {
                        Name = "T",
                        Value = Random.NextDouble()*200,
                        Units = "°C"
                    },
                    new InfoParamDto
                    {
                        Name = "P",
                        Value = 40 + Random.NextDouble()*100,
                        Units = "кгс/см²"
                    }
                }
            };
            int sepNumber = 4 + Random.Next(3);
            for (int i = 0; i < sepNumber; i++) {
                collector.Separators.Add(createTestSeparator(201 + i, collector));
            }
            var jumpersCount = Random.Next(sepNumber);
            for (int i = 0; i < jumpersCount; i++) {
                var jumper = new SeparatorJumperDto
                {
                    Choke = new ChokeDto
                    {
                        Params = new List<InfoParamDto>
                        {
                            new InfoParamDto
                            {
                                Name = "ø", //alt + 0248
                                Value = 15,
                                Units = "мм"
                            }
                        }
                    }
                };
                var sep1Idx = Random.Next(sepNumber);
                var sep2Idx = Random.Next(sepNumber);
                while (sep1Idx == sep2Idx) {
                    sep2Idx = Random.Next(sepNumber);
                }
                jumper.Separator1 = collector.Separators[sep1Idx];
                jumper.Separator2 = collector.Separators[sep2Idx];
                collector.SeparatorJumpers.Add(jumper);
            }
            return collector;
        }

        public static NetworkDto createTestGroup() {
            var group = new NetworkDto
            {
                Name = "УКПГ-1",
                Params = new List<InfoParamDto>
                {
                    new InfoParamDto
                    {
                        Name = "Qгс",
                        Value = Random.NextDouble()*200,
                        Units = "м³/сут"
                    },
                    new InfoParamDto
                    {
                        Name = "Qнк",
                        Value = 40 + Random.NextDouble()*100,
                        Units = "т/сут"
                    },
                    new InfoParamDto
                    {
                        Name = "Тнтс",
                        Value = 500 + Random.NextDouble()*500,
                        Units = "°C"
                    }
                }
            };
            group.Collectors.Add(createTestCollector("ЗПА", 0));
            if (Random.NextDouble() >= 0.5) {
                group.Collectors.Add(createTestCollector("ППА", 1));
            }

//            group.Collectors.Add(createTestCollector("ППА", 1));
//            if (Random.NextDouble() >= 0.3) {
//                group.Collectors.Add(createTestCollector("КПА", 2));
//            }
//            if (Random.NextDouble() >= 0.3) {
//                group.Collectors.Add(createTestCollector("ЛПА", 3));
//            }

            return group;
        }

        public static List<VievListItem> createTestLegendItems() {
            return new List<VievListItem>
            {
                new VievListItem
                {
                    Text = "УКПГ",
                    IsChecked = true
                },
                new VievListItem
                {
                    Text = "ППА",
                    IsChecked = true
                },
                new VievListItem
                {
                    Text = "ЗПА",
                    IsChecked = false
                },
                new VievListItem
                {
                    Text = "Куст",
                    IsChecked = true
                },
                new VievListItem
                {
                    Text = "Штуцер",
                    IsChecked = false
                },
                new VievListItem
                {
                    Text = "Скважина",
                    IsChecked = false
                }
            };
        }
    }
}