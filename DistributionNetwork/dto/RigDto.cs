﻿using System.Collections.Generic;

namespace DistributionNetwork.dto {
    /// <summary>
    /// Куст
    /// </summary>
    public class RigDto : InfoDto {
        public List<WellDto> Wells { get; set; }

        public RigDto() {
            Wells = new List<WellDto>();
        }
    }
}
