﻿namespace DistributionNetwork.dto {
    /// <summary>
    /// Скважина
    /// </summary>
    public class WellDto : InfoDto {

        /// <summary>
        /// Информация о штуцере
        /// </summary>
        public ChokeDto ChokeInfo { get; set; }
    }
}
