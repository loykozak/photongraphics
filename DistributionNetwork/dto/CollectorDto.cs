﻿using System.Collections.Generic;

namespace DistributionNetwork.dto
{
    /// <summary>
    /// Коллектор
    /// </summary>
    public class CollectorDto : InfoDto
    {
        public int OrderNumber { get; set; }
        public List<SeparatorDto> Separators { get; set; }
        public List<SeparatorJumperDto> SeparatorJumpers { get; set; }

        public CollectorDto() {
            Separators = new List<SeparatorDto>();
            SeparatorJumpers = new List<SeparatorJumperDto>();
        }

    }
}
