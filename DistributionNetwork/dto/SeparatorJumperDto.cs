﻿namespace DistributionNetwork.dto
{
    /// <summary>
    /// Перемычка между сепараторами
    /// </summary>
    public class SeparatorJumperDto
    {
        public ChokeDto Choke { get; set; }

        public SeparatorDto Separator1 { get; set; }

        public SeparatorDto Separator2 { get; set; }
    }
}
