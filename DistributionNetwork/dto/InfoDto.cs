﻿using System.Collections.Generic;

namespace DistributionNetwork.dto {
    /// <summary>
    /// Состояние объекта
    /// </summary>
    public enum ObjectState {
        /// <summary>
        /// в работе
        /// </summary>
        osEnabled,

        /// <summary>
        /// не в работе
        /// </summary>
        osDisabled,

        /// <summary>
        /// неисправен
        /// </summary>
        osAlarm
    }

    /// <summary>
    /// Базовый класс информации об элементе
    /// </summary>
    public class InfoDto {
        public ObjectState State { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public List<InfoParamDto> Params { get; set; }
    }

    /// <summary>
    /// Класс с информацией о параметре элемента
    /// </summary>
    public class InfoParamDto {
        public string Name { get; set; }
        public double Value { get; set; }
        public string Units { get; set; }
    }

    public static class ObjectStateExtension {
        public static ObjectState nextState(this ObjectState state) {
            switch (state) {
                case ObjectState.osEnabled:
                    return ObjectState.osDisabled;
                case ObjectState.osDisabled:
                    return ObjectState.osAlarm;
                default:
                    return ObjectState.osEnabled;
            }
        }
    }
}