﻿using System.Collections.Generic;

namespace DistributionNetwork.dto {
    /// <summary>
    /// Шлейф
    /// </summary>
    public class SeparatorDto : InfoDto {
        public CollectorDto Collector { get; set; }
        public List<WellDto> Wells { get; set; }
        public List<RigDto> Rigs { get; set; }

        public SeparatorDto() {
            Wells = new List<WellDto>();
            Rigs = new List<RigDto>();
        }
    }
}