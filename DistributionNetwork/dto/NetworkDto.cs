﻿using System.Collections.Generic;

namespace DistributionNetwork.dto {
    /// <summary>
    /// УКПГ
    /// </summary>
    public class NetworkDto : InfoDto {
        public List<CollectorDto> Collectors { get; set; }

        public NetworkDto() {
            Collectors = new List<CollectorDto>();
        }
    }
}
