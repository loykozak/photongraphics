﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DistributionNetwork.gdi;
using DistributionNetwork.styles;
using PhotonGraphics.elements.baseElements;

namespace DistributionNetwork {
    public partial class FormTestGdi : Form {
        private Network _groupGdi;
        private readonly ScaleButtons _scaleButtons;

        public FormTestGdi() {
            InitializeComponent();

            canvas1.BackColor = StyleCollection.backgroundColor;

            //Создание кнопок управления масштабом
            _scaleButtons = new ScaleButtons();
            Resize += (_, __) => updateScaleButtonsPosition();
            _scaleButtons.OnFullSizeButtonClick += (_, __) => canvas1.fitSize();
            _scaleButtons.OnPlusButtonClick += (_, __) => canvas1.increaseZoom();
            _scaleButtons.OnMinusButtonClick += (_, __) => canvas1.decreaseZoom();

            //            renderWellList();
            //            renderRig();
            //            renderSeparator();
            renderGroup();
        }

        #region Test render

        private void renderGroup() {
            var groupDto = TestDtoGenerator.createTestGroup();
            _groupGdi = new Network(groupDto);
            propertyGrid1.SelectedObject = groupDto;

            //Создание легенды
            var legend = new ViewList
            {
                Title = "Информация по объектам:",
                Items = TestDtoGenerator.createTestLegendItems(),
                Location = new PointF(20, 20)
            };
            _groupGdi.childs.Add(legend);

            //Добавление кнопок управления масштабом
            _groupGdi.childs.Add(_scaleButtons);

            canvas1.RenderElement = _groupGdi;
        }

        private void renderWellList()
        {
            var dto = TestDtoGenerator.createTestWellList();
            var gdi = new WellList(dto, false, 0);

            //Добавление кнопок управления масштабом
            gdi.childs.Add(_scaleButtons);

            canvas1.RenderElement = gdi;
        }

        private void renderRig()
        {
            var dto = TestDtoGenerator.createTestRig(201);
            var gdi = new Rig(dto, false, 0);

            //Добавление кнопок управления масштабом
            gdi.childs.Add(_scaleButtons);

            canvas1.RenderElement = gdi;
        }

        private void renderSeparator()
        {
            var dto = TestDtoGenerator.createTestSeparator(201, TestDtoGenerator.createTestCollector("PPA", 0));
            var gdi = new Separator(dto, false);

            //Добавление кнопок управления масштабом
            gdi.childs.Add(_scaleButtons);

            canvas1.RenderElement = gdi;
        }

        #endregion

        /*
        private void selectDtoObject(object dto) {
            _groupGdi.Enabled = dto == null;
            if (dto != null) {
                var el = findElement(_groupGdi, dto);
                if (el != null) {
                    el.Enabled = true;
                }
            }
            canvas1.Refresh();
        }*/

        private GdiElement findElement(GdiElement currentElement, object source) {
            if (currentElement is Network) {
                if (((Network) currentElement).source == source) {
                    return currentElement;
                }
            }
            if (currentElement is Separator) {
                if (((Separator) currentElement).source == source) {
                    return currentElement;
                }
            }
            if (currentElement is Rig) {
                if (((Rig) currentElement).source == source) {
                    return currentElement;
                }
            }
            if (currentElement is Well) {
                if (((Well) currentElement).source == source) {
                    return currentElement;
                }
            }
            if (currentElement is GdiGroup) {
                var group = (GdiGroup) currentElement;
                return @group.childs.Select(child => findElement(child, source)).FirstOrDefault(res => res != null);
            }
            return null;
        }

        private void updateScaleButtonsPosition() {
            const float offset = 10f;
            _scaleButtons.Location = new PointF(canvas1.Width - offset - _scaleButtons.Size.Width,
                canvas1.Height - offset - _scaleButtons.Size.Height);
        }

        #region Обработчики

        /*
        private void onObjectSelected(object sender, bool selected) {
            if (sender == _groupGdi.source ||
                !selected) {
                propertyGrid1.SelectedObject = _groupGdi.source;
                selectDtoObject(null);
            } else {
                propertyGrid1.SelectedObject = sender;
                selectDtoObject(sender);
            }
        }*/

        private void chkMouseScrollCheckedChanged(object sender, EventArgs e) {
            canvas1.AllowMouseTranslate = chkMouseScroll.Checked;
        }

        private void chkMouseZoomCheckedChanged(object sender, EventArgs e) {
            canvas1.AllowMouseZoom = chkMouseZoom.Checked;
        }

        private void btnFitSizeClick(object sender, EventArgs e) {
            canvas1.fitSize();
        }

        private void formTestGdiLoad(object sender, EventArgs e) {
            canvas1.fitSize();
        }

        #endregion
    }
}