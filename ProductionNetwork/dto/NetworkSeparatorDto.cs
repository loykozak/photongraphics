﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionNetwork.dto {
    public class NetworkSeparatorDto : InfoDtoLocation {
        public int Num { get; set; }
    }
}
