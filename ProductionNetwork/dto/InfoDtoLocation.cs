﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using DistributionNetwork.dto;

namespace ProductionNetwork.dto {
    public class InfoDtoLocation : InfoDto {
        public PointF Location { get; set; }
        public PointF Start { get; set; }
        public PointF End { get; set; }

    }
}
