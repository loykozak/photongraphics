﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionNetwork.dto {
    public class ConnPointDto : InfoDtoLocation {
        public bool Active { get; set; }
    }
}
