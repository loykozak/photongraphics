﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistributionNetwork.dto;
using ProductionNetwork.gdi;

namespace ProductionNetwork.dto {
    public class GraphDto : InfoDtoLocation {
        public List<UkpgDto> UkpgList;
        public List<NetworkSeparatorDto> SeparatorList;
        public List<NetworkWellDto> WellList;
        public List<PipeDto> PipeList;
        public List<GatherPointDto> GatherList;

        public List<ConnPointDto> PointList;
        public List<ChockeDto> ChokeList;
        

    }
}
