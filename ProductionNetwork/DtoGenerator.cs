﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using PhotonGraphics.elements.baseElements;
using ProductionNetwork.dto;
using ProductionNetwork.gdi;

namespace ProductionNetwork {
    public static class DtoGenerator {
        private static Random rnd = new Random();

        public static List<PipeDto> getPipeDtoList() {
            var ret = new List<PipeDto>();

            for (int i = 0; i < 10; i++) {
                ret.Add(new PipeDto() {
                    Start =  new PointF( (float)(rnd.NextDouble()*100-50), (float)(rnd.NextDouble() * 100 - 50)),
                    End =  new PointF( (float)(rnd.NextDouble()*100-50), (float)(rnd.NextDouble() * 100 - 50))
                });
            }

            return ret;
        }

    }
}
