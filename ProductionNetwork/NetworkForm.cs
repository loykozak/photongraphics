﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PhotonGraphics.elements.shapes;
using ProductionNetwork.dto;
using ProductionNetwork.gdi;

namespace ProductionNetwork {
    public partial class NetworkForm : Form {
        public NetworkForm() {
            InitializeComponent();
            testRenderer();
        }

        private void testRenderer() {
            canvas.RenderElement =
                new Graph(new GraphDto() {
                    UkpgList =
                        new List<UkpgDto>() {
                            new UkpgDto() {Title = "УКПГ-1", Location = new PointF(0, -200)},
                            new UkpgDto() {Title = "УКПГ-2", Location = new PointF(0, 200)}
                        },
                    SeparatorList = new List<NetworkSeparatorDto>() {
                        new NetworkSeparatorDto() { Title = "ЗПА", Num = 101,Location = new PointF(400,-300)},
                        new NetworkSeparatorDto() { Title = "ЗПА", Num = 102,Location = new PointF(400,-150)},
                        new NetworkSeparatorDto() { Title = "ЗПА", Num = 103,Location = new PointF(400,0)},
                        new NetworkSeparatorDto() { Title = "ЗПА", Num = 104,Location = new PointF(400,150)},
                        new NetworkSeparatorDto() { Title = "ЗПА", Num = 105,Location = new PointF(400,300)},
                        new NetworkSeparatorDto() { Title = "ЗПА", Num = 106,Location = new PointF(400,400)},
                    }, 
                    PipeList = new List<PipeDto>() {
                        new PipeDto() { End =  new PointF(0, -200), Start = new PointF(400,-300)},
                        new PipeDto() { End =  new PointF(0, -200), Start = new PointF(400,-150)},
                        new PipeDto() { End =  new PointF(0,  200), Start = new PointF(400, 0 )},
                        new PipeDto() { End =  new PointF(0,  200), Start = new PointF(400, 150 )},
                        new PipeDto() { End =  new PointF(0,  200), Start = new PointF(400, 300 )},
                        new PipeDto() { End =  new PointF(0,  200), Start = new PointF(400, 400 )},
                        new PipeDto() { End =  new PointF(600, 550), Start = new PointF(600, 700)},
                        new PipeDto() { End =  new PointF(0,  200), Start = new PointF(400, 500)},
                        new PipeDto() { End =  new PointF(400, 500), Start = new PointF(600, 550)},
                        new PipeDto() { End =  new PointF(600, 550), Start = new PointF(900, 480)},
                        new PipeDto() { End =  new PointF(900, 480), Start = new PointF(1000, 600)},
                        new PipeDto() { End =  new PointF(400,-300), Start = new PointF(1000, -300)},
                        new PipeDto() { End =  new PointF(1000, -300), Start = new PointF(900, -500)},

                        new PipeDto() { End =  new PointF(430, -700), Start = new PointF(430, -800)},
                        new PipeDto() { End =  new PointF(900, -500), Start = new PointF(430, -700)},

                          new PipeDto() { End =  new PointF(900, 480), Start = new PointF(1100, 280)},


                    },
                    GatherList = new List<GatherPointDto>() {
                        new GatherPointDto() {Num = 101, Location = new PointF(430, -800)},
                        new GatherPointDto() {Num = 109, Location = new PointF(600, 700)},
                        new GatherPointDto() {Num = 109, Location = new PointF(1000, 600)}
                    },
                    PointList = new List<ConnPointDto>() {
                        new ConnPointDto() {Location = new PointF(600, 550), Active = true},
                        new ConnPointDto() {Location = new PointF(400, 500), Active = false},
                        
                        new ConnPointDto() {Location = new PointF(1000, -300), Active = false},
                        new ConnPointDto() {Location = new PointF(900, -500), Active = true},
                        new ConnPointDto() {Location = new PointF(430, -700), Active = false},

                        new ConnPointDto() {Location = new PointF(900, 480), Active = false},
                        new ConnPointDto() {Location = new PointF(1100, 280), Active = false},

                    },
                    ChokeList = new List<ChockeDto>() {
                        new ChockeDto() { Start = new PointF(1000,380), End = new PointF(1050,330)}
                    },
                    WellList = new List<NetworkWellDto>() {
                        new NetworkWellDto() {Location =new PointF(1100, 280), Title = "1012"}
                    }

                });

            //new GdiDirectPipe(new PipeDto() {End =  new PointF(1000,2000), Start = new PointF(0,0)});
        }

        private void NetworkForm_Load(object sender, EventArgs e) {
            canvas.fitSize();
        }
    }
}
