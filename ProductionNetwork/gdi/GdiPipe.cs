﻿using System;
using System.Collections.Generic;
using System.Drawing;
using PhotonGraphics.elements.baseElements;
using PhotonGraphics.elements.shapes;
using ProductionNetwork.dto;

namespace ProductionNetwork.gdi {
    public class GdiPipe : GdiGroup {

        private readonly Color PIPE_COLOR = Color.FromArgb(95,129,167);
        private float PIPE_WIDTH = 5;
        private readonly SizeF ARROW_SIZE = new SizeF(20,20);
        private readonly float PROCENT_OFFSET = 0.85f;

        public GdiPipe(PipeDto source) {
            
            var pipe = new GdiPolyline() {
                Points = new List<PointF>(2) {source.Start, source.End},
                Pen = new Pen(PIPE_COLOR) {Width = PIPE_WIDTH},
                //HasConstantLineThickness = true
            };
            childs.Add(pipe);

           /* var vector = new PointF(source.End.X - source.Start.X, source.End.Y - source.Start.Y);
            var arrowAngle = Math.Atan2(vector.Y, vector.X) - Math.Atan2(0,1);

            var arrowLocation = new PointF(vector.X*PROCENT_OFFSET + source.Start.X, vector.Y*PROCENT_OFFSET+source.Start.Y);
            var arrow = new GdiArrow() {
                Location = arrowLocation,
                Angle = (float)(arrowAngle*180f/Math.PI),
                Color = PIPE_COLOR, 
                Height = ARROW_SIZE.Height, 
                Width = ARROW_SIZE.Width, 
                MiddleInset = 0,
            };
            childs.Add(arrow);*/
            Tag = source;
        }

    }
}
