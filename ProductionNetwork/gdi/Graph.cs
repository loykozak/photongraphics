﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhotonGraphics.elements.baseElements;
using ProductionNetwork.dto;

namespace ProductionNetwork.gdi {
    public class Graph : GdiGroup {


        public Graph(GraphDto source) {

           foreach (var pipeDto in source.PipeList) {
                childs.Add(new GdiPipe(pipeDto));
            }
            
            foreach (var ukpg in source.UkpgList) {
                childs.Add(new GdiUkpg(ukpg));
            }

            foreach (var separatorDto in source.SeparatorList) {
                childs.Add(new GdiSeparator(separatorDto));
            }

            foreach (var gatherPointDto in source.GatherList) {
                childs.Add(new GdiGatherPoint(gatherPointDto));
            }

            foreach (var chockeDto in source.ChokeList) {
                childs.Add(new GdiChocke(chockeDto));
            }

            foreach (var pointDto in source.PointList) {
                childs.Add(new GdiConnectionPoint(pointDto));
            }

            foreach (var wellDto in source.WellList) {
                childs.Add(new GdiWell(wellDto));
            }
            //childs.Add(new GdiNetworkEllipse() {});

          /*  childs.Add(new GdiDirectPipe(new PipeDto() { Start = new PointF(0, 0), End = new PointF(100, 500) }));
            childs.Add(new GdiConnectionPoint() { Location = new PointF(0, 0), Radius = 100, Acitve = true });
            childs.Add(new GdiConnectionPoint() { Location = new PointF(100, 500), Radius = 100, Acitve = false });*/
            
        }

    }
}
