﻿using System.Drawing;
using PhotonGraphics.elements.baseElements;
using PhotonGraphics.elements.shapes;
using ProductionNetwork.dto;

namespace ProductionNetwork.gdi {
    public class GdiWell : GdiShape {

        public bool Hovered { get; set; }
        private static readonly Color DiffalutColor = Color.Aqua;
        private static readonly Color HoverColor = Color.DarkTurquoise;
        private static readonly Size DefSize  = new Size(100,30);
        protected static readonly Font TextFont = new Font("Segoe UI", 16, FontStyle.Regular);
        protected static readonly Brush TxtBrush = Brushes.White;


        public GdiWell(NetworkWellDto source) {
            DrawBorder = false;
            ShapeBrush = new SolidBrush(DiffalutColor);
            MouseHover += (_, __) => ShapeBrush = new SolidBrush(HoverColor);
            MouseLeave += (_, __) => ShapeBrush = new SolidBrush(DiffalutColor);
            MouseDown += (_, __) => ShapeBrush = new SolidBrush(HoverColor);
            MouseUp += (_, __) => ShapeBrush = new SolidBrush(HoverColor);
            Size = DefSize;
            Center = source.Location;
           
            Font = TextFont;
            TextBrush = TxtBrush;
            Tag = source;
            Text = source.Title;
        }

        public override void render(LibGraphicsObject go) {
            go.Graphics.FillRectangle(ShapeBrush, RenderingRect);
            base.render(go);
        }
    }
}
