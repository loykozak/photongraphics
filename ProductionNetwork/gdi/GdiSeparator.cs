﻿using System.Drawing;
using System.Windows.Forms;
using PhotonGraphics.elements.baseElements;
using PhotonGraphics.elements.shapes;
using ProductionNetwork.dto;

namespace ProductionNetwork.gdi {
    public class GdiSeparator : GdiNetworkEllipse {
        public override Color DiffalutColor
        {
            get { return Color.FromArgb(95,128,166); }
        }
        public override Color HoverColor
        {
            get {  return Color.FromArgb(95, 128, 166); }
        }

        private GdiImage gdiImage;


        public GdiSeparator(NetworkSeparatorDto source) {

            // this.Text = source.Num.ToString();
            
           
          /*  TextBlocks = new List<TextBlockInfo>();
            TextBlocks.Add(new TextBlockInfo() { Brush = TxtBrush, Font = TextFont, Text = source.Num.ToString() + '\n' });
            TextBlocks.Add(new TextBlockInfo() { Brush = TxtBrush, Font = TextFont, Text = source.Title });*/
            
            this.Radius = 20;
            this.Center = source.Location;
            gdiImage = new GdiImage();
            gdiImage.Image = Properties.Resources.btn_separator;
            gdiImage.SizeMode = PictureBoxSizeMode.CenterImage;
            gdiImage.Size = Size;
           // HasConstantSize = true;
           // AnchorPoint = Center;
           // gdiImage.Location = source.Location;
            Tag = source;

        }

        public override void render(LibGraphicsObject go) {

            var rect = RenderingRect;
            if (DrawFill && ShapeBrush != null) {
                go.Graphics.FillEllipse(ShapeBrush, rect);
            }
            if (DrawBorder && BorderPen != null) {
                go.Graphics.DrawEllipse(getBorderPen(BorderPen, go), rect);
            }
            gdiImage.render(go);
            base.render(go);
        }
    }
}
