﻿using System.Drawing;
using PhotonGraphics.elements.shapes;

namespace ProductionNetwork.gdi {
    public class GdiNetworkEllipse : GdiEllipse {
        public bool Hovered { get; set; }
        public virtual  Color DiffalutColor { get; set; }
        public virtual Color HoverColor { get; set; }
        protected static readonly Font TextFont  = new Font("Segoe UI", 12, FontStyle.Bold);
        protected static readonly Brush TxtBrush = Brushes.White;

        public float Radius { get; set; }

        public override SizeF Size {
            get { return new SizeF(Radius*2, Radius*2); }
            set { Radius = value.Width/2; }
        }

        /*  protected PointF _location;


          public override SizeF Size
          {
              get { return new SizeF(Radius * 2, Radius * 2); }
              set { }
          }

          public override PointF Location
          {
              get { return new PointF(_location.X + Radius, _location.Y + Radius); }
              set { _location = new PointF(value.X - Radius, value.Y - Radius); }
          }

          public override RectangleF BoundRect
          {
              get { return new RectangleF(_location, Size); }
              set
              {
                  Location = value.Location;
                  Size = value.Size;
              }
          }*/

        public GdiNetworkEllipse() {
          
        
            BorderPen = null;
      
            ShapeBrush = new SolidBrush(DiffalutColor);
            MouseHover += (_, __) => ShapeBrush = new SolidBrush(HoverColor);
            MouseLeave += (_, __) => ShapeBrush = new SolidBrush(DiffalutColor);
            MouseDown += (_, __) => ShapeBrush = new SolidBrush(HoverColor);
            MouseUp += (_, __) => ShapeBrush = new SolidBrush(HoverColor);
        }


    }
}
