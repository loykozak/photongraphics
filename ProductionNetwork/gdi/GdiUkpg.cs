﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductionNetwork.dto;

namespace ProductionNetwork.gdi {
    public class GdiUkpg : GdiNetworkEllipse {
        public override Color DiffalutColor {
            get  {return Color.SteelBlue; }
        }
        public override Color HoverColor
        {
            get { return Color.DarkBlue; }
        }

      
        public GdiUkpg(UkpgDto source) {
           
            this.Text = source.Title;
            this.TextBrush = TxtBrush;
            Font = TextFont;
            this.Radius = 50;
            this.Center = source.Location;
            Tag = source;
           
        }
    }
}
