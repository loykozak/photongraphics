﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhotonGraphics.elements.shapes;
using ProductionNetwork.dto;

namespace ProductionNetwork.gdi {
    public class GdiGatherPoint : GdiNetworkEllipse {
        public override Color DiffalutColor
        {
            get { return Color.SandyBrown; }
        }
        public override Color HoverColor
        {
            get { return Color.SaddleBrown; }
        }

        private static readonly Font GatherFont = new Font("Segoe UI", 12, FontStyle.Regular);

        public GdiGatherPoint(GatherPointDto source) {

            TextBlocks = new List<TextBlockInfo>();
            TextBlocks.Add(new TextBlockInfo() { Brush = TxtBrush, Font = GatherFont, Text = "Шлейф\n" });
            TextBlocks.Add(new TextBlockInfo() { Brush = TxtBrush, Font = TextFont, Text = source.Num.ToString()  });

            this.Radius = 40;
            this.Center = source.Location;
            Tag = source;

        }
    }
}
