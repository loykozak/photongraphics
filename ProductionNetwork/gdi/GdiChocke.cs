﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhotonGraphics.elements.baseElements;
using PhotonGraphics.elements.shapes;
using ProductionNetwork.dto;

namespace ProductionNetwork.gdi {
    public class GdiChocke : GdiGroup {

        private readonly Color PIPE_COLOR = Color.SteelBlue;
        private readonly SizeF ARROW_SIZE = new SizeF(25, 30);

        public GdiChocke(ChockeDto source) {
            var vector = new PointF(source.End.X - source.Start.X, source.End.Y - source.Start.Y);
            var arrowAngle = Math.Atan2(vector.Y, vector.X) - Math.Atan2(0, 1);

            var arrowLocation = new PointF((source.End.X + source.Start.X)/2, (source.End.Y + source.Start.Y) / 2);

            var arrow1 = new GdiArrow() {
                Location = arrowLocation,
                Angle = (float)(arrowAngle * 180f / Math.PI),
                Color = PIPE_COLOR,
                Height = ARROW_SIZE.Height,
                Width = ARROW_SIZE.Width,
                MiddleInset = 0,
            };
            var arrow2 = new GdiArrow() {
                Location = arrowLocation,
                Angle = (float)(arrowAngle * 180f / Math.PI)+180,
                Color = PIPE_COLOR,
                Height = ARROW_SIZE.Height,
                Width = ARROW_SIZE.Width,
                MiddleInset = 0,
            };
            childs.Add(arrow1);
            childs.Add(arrow2);
            Tag = source;

        }


    }
}
