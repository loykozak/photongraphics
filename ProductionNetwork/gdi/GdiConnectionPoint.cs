﻿using System.Drawing;
using PhotonGraphics.elements.baseElements;
using ProductionNetwork.dto;

namespace ProductionNetwork.gdi {
    public class GdiConnectionPoint : GdiActiveElement {
    
        private static readonly Color ACTIVE_COLOR = Color.Aqua;
        private static readonly Color DISABLE_COLOR = Color.LightGray;
        private static float OUT_WIDTH = 0.2f;
        private static float INNER_RADIUS = 0.6f;
        
        
        public float Radius { get; set; }
        public override SizeF Size {
            get { return new SizeF(Radius*2, Radius*2);}
            set { }
        }

        public override void render(LibGraphicsObject go) {
            render(go.Graphics);
        }
        
        public bool Acitve { get; set; }

        public GdiConnectionPoint(ConnPointDto source) {
            Acitve = source.Active;
            Radius = 15;
            Center = source.Location;
            Tag = source;
        }

        private void render(Graphics g) {
            var b1 = new SolidBrush(ACTIVE_COLOR);
            var b2 = new SolidBrush(DISABLE_COLOR);

            var innerRadius = Radius * (INNER_RADIUS - 1);
            var rect = RenderingRect;
            rect.Inflate(innerRadius, innerRadius);

            if (Acitve) {
                var width = (int) Radius*OUT_WIDTH;
                var outRadius = -Radius* OUT_WIDTH/2 ;
                var rect2 = RenderingRect;
                rect2.Inflate(outRadius, outRadius);
                g.FillEllipse(b1, rect);
                g.DrawEllipse(new Pen(ACTIVE_COLOR) {Width = width}, rect2 );
            } else {
                g.FillEllipse(b2, rect);
            }
            
         }

    }
}
